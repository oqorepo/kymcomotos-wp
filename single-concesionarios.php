<?php get_template_part('includes/header'); ?>

<section class="container bk-section pt-5 pb-5">
    <?php if(have_posts()): while(have_posts()): the_post(); ?>
    <h2 class="text-center"><?php the_title(); ?></h2>
    <ul id="post_<?php the_ID()?>" <?php //post_class()?> class="row">
        <li class="bk-dealer--content col-sm" id="<?php echo strtolower(str_replace(' ', '', the_title('', '', false))); ?>">
        <?php if( have_rows('cn-datos') ): while( have_rows('cn-datos') ): the_row();
        $image = get_sub_field('cn-img');
        $link = get_sub_field('cn-tel');
        $mail = get_sub_field('cn-email'); 
        $colors = get_sub_field('cn-service-type');
        $location = get_field('cn-map');
        ?>

            <?php if( !empty($image) ): ?>
            <div class="bk-dealer--card__img pt-4">
                <img src="<?php echo $image['url']; ?>" alt="" class="w-100">
            </div>
            <?php endif; ?>

            <div class="bk-dealer--card__txt">
                <p class="cn-address"><b>Dirección:</b><br><?php echo $location['address']; ?></p>

                <?php if( have_rows('cn-tel') ): ?>
                <p class="cn-tel"><b>Teléfonos:</b><br>
                <?php while( have_rows('cn-tel') ): the_row();
                    $content = get_sub_field('cn-tel-rp');
                    ?>
                    <a href="<?php echo $content ?>"> <?php echo $content ?> </a>
                    <?php endwhile; ?>
                </p>
                <?php endif; ?>

                <?php if( have_rows('cn-email') ): ?>
                <p class="cn-email"><b>Email:</b><br>
                    <?php while( have_rows('cn-email') ): the_row(); 
                    $mailings = the_sub_field('cn-email-rp'); 
                ?>
                    <a href="mailto:<?php echo $mailings ?>"><?php echo $mailings ?></a>
                    <?php endwhile; ?>
                </p>
                <?php endif; ?>

                <?php if( $colors ): ?>
                    <p class="cn-service"><b>Tipos de servicios:</b><br></p>
                <ul class="d-flex">
                    <?php foreach( $colors as $color ): ?>
                    <li class="cn-<?php echo $color; ?> cn-service-icon"><p><?php echo $color; ?></p></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>

            </div>
        <?php endwhile; endif; ?>
        </li>

        <li class="col-sm">
            <?php if( !empty($location) ): ?>
            <div class="acf-map">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                    <h4><a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_title(); ?></a></h4> <!-- Output the title -->
                    <p class="address"><?php echo $location['address']; ?></p> <!-- Output the address -->
                </div>
            </div>
            <?php endif; ?>
        </li>
    </ul>
<?php
  endwhile; wp_reset_postdata(); else :
    get_template_part('./includes/loops/404');
  endif;
?>
</section>
<hr>
<?php
    get_template_part('includes/loops/slides/home-product');
    get_template_part('includes/footer');
?>
<style type="text/css">
.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	margin: 20px 0;
}
/* fixes potential theme css conflict */
.acf-map img {
   max-width: inherit !important;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7AwzK3GDV8fAPxY5CXMmUUMIvLPSkgk0"></script>
<script type="text/javascript">
(function($) {
function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
        mapTypeId	: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    };
    
	// create map
	var map = new google.maps.Map( $el[0], args);
	
	// add a markers reference
    map.markers = [];
    
	// add markers
	$markers.each(function(){		
    	add_marker( $(this), map );		
	});
	// center map
	center_map( map );
	// return
	return map;	
}
/*
*  add_marker
*/
function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
        map			: map,
        icon: 'https://www.kymcomotos.cl/wp-content/uploads/2019/07/bk-marker-icon.png'
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}
}

/*
*  document ready
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
</script>