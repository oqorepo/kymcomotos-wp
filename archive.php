<?php get_template_part('includes/header'); ?>

<section class="container mt-5">
  <?php get_template_part('includes/loops/index-loop'); ?>
</section><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
