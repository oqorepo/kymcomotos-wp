<?php get_template_part('includes/header'); ?>
<section class="container mt-5 mb-5">
  <div class="row">

    <div class="col-sm-12">
      <header class="mb-4 border-bottom">
          <h4 class="text-center bk--title">
            <?php _e('Resultados de búsqueda para:', 'b4st'); ?> <span class="bk--title__i">"<?php the_search_query(); ?>"</span>
          </h4>
      </header>
    </div>
    <?php get_template_part('includes/loops/search-results'); ?>

    <?php //get_template_part('includes/sidebar'); ?>

  </div><!-- /.row -->
</section><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
