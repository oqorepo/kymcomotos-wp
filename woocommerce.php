<?php 
get_template_part('includes/header');

if( is_product()){

  global $product;

  if ($product->is_type('simple')) {
    $precio_normal  = (int)get_post_meta(get_the_ID(), '_regular_price', true);;
    $precio_oferta  = (int)get_post_meta(get_the_ID(), '_sale_price', true);
    $valor_bono_financiamiento = (get_field('valor_bono')) ? get_field('valor_bono') : 0;
    if ($precio_oferta) {
        $valor_bono = ($precio_normal-$precio_oferta);
    } else {
        $valor_bono = (int)0;
    }
    $precio_final   = ($precio_normal-$valor_bono);

    if (!empty($valor_bono_financiamiento)) {
      $precio_final = ($precio_final - $valor_bono_financiamiento);
    }
  }

  if ($product->product_type == 'variable') {
    #Step 1: Get product varations
    $available_variations = $product->get_available_variations();
    #Step 2: Get product variation id
    $variation_id = $available_variations[0]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
    #Step 3: Create the variable product object
    $variable_product1 = new WC_Product_Variation($variation_id);
    #Step 4: You have the data. Have fun :)

    $precio_normal = $variable_product1->regular_price;
    $precio_oferta = $variable_product1->sale_price;
    $valor_bono_financiamiento =  (get_field('valor_bono')) ? get_field('valor_bono') : 0;
    if ($precio_oferta) {
        $valor_bono = ($precio_normal-$precio_oferta);
    } else {
        $valor_bono = (int)0;
    }

    $precio_final   = ($precio_normal-$valor_bono);

    if (!empty($valor_bono_financiamiento)) {
      $precio_final = ($precio_final - $valor_bono_financiamiento);
    }
  }


echo '
<nav class="bk-fixed-nav container-fluid navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <div class="row w-100">
      <div class="col-lg-4">
        <div class="d-flex">
          <button class="cd-nav-trigger bk-woocommerce--shop__btn bk--btn bk--btn__primary bk--btn__small mx-2">Cotizar ' . $product->name . ' <i class="fas fa-chevron-right"></i></button>
          <button id="btn-hide" class="bk--btn bk--btn__primary bk--btn__small d-lg-none ml-auto">Detalle <i class="fas fa-chevron-down"></i></button>
        </div>
      </div>
      <div class="col-lg-8">
        <ul class="active-info hide-detail w-100 d-lg-flex align-items-center justify-content-between">
          <li class="text-center pt-2 pt-lg-0 head-valfinal">
            <h2 style="margin:0;">$ ' . number_format($precio_final, 0, ",", ".") . '</h2>
            <div class="valor-cae" style="color:#41b451;">
              <small class="cae" ></small>
              <small class="price"></small>
            </div>
          </li>
          ';
if(!empty($valor_bono) || $valor_bono != 0){
echo'
            <li>
              <div class="bono head-valbono text-center">
                <small>Incluye bono:</small>
                <br>
                <small style="color:#41b451; font-weight:900">$ ' . number_format($valor_bono, 0, ",", ".") . '</small>
              </div>
            </li>
';
}
if(!empty($valor_bono_financiamiento) || $valor_bono_financiamiento != 0){
echo'
          <li>
            <div class="bono head-valbono-financiamiento text-center" data-bonoFinanciamiento="'. number_format($valor_bono_financiamiento, 0, ",", "") .'">
              <small>Bono Financiamiento:</small>
              <br>
              <small style="color:#41b451; font-weight:900">$ ' . number_format($valor_bono_financiamiento, 0, ",", ".") . '</small>
            </div>
          </li>
';
}
if(!empty($valor_bono) || !empty($valor_bono_financiamiento)){
  echo'
          <li>
            <div class="bono head-valnormal text-center">
              <small>Precio Normal:</small>
              <br>
              <small style="color:#41b451; font-weight:900"><span>$ ' . number_format($precio_normal, 0, ",", ".") . '</span></small>
            </div>
          </li>
  ';
}
echo'
        </ul>
      </div>
    </div>
  </div>
</nav>
  ';

}
?>
<div class="bk-woocommerce">
  <?php woocommerce_content(); ?>
</div>

<?php 
if( is_product() ) {
//get_template_part('includes/section-v');
echo '
<div class="container-fluid mt-5 mb-5">
  <div class="row text-center">
    <div class="col-12">
      <div class="bk--title">
          <h2 class="text-center">Las mejores Experiencias<span class="bk--title__i"> kymco</span></h2>
          <p class="text-center">- Better Than Best -</p>
      </div>
    </div>
    <div class="col-12">
';
$instagram_feed = get_field('shortcode_instagram_feed');
if ($instagram_feed){
  echo do_shortcode($instagram_feed);
}
echo '
    </div>
  </div>
</div>
';
}
get_template_part('includes/section-c');
get_template_part('includes/footer'); 
?>