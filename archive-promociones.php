<?php get_template_part('includes/header'); ?>

<?php if(have_posts()) : ?>
<?php while(have_posts()) : the_post(); 
$enlace = get_field('bk_enlace_externo');
    if ($enlace){
        echo '<a href="'. $enlace. '">';
        echo the_post_thumbnail('full', ['class'=>'w-100 bk-promo-img']);
        echo '</a>';
    }else {
        echo '<a href="'. the_permalink() .'">';
        echo the_post_thumbnail('full', ['class'=>'w-100 bk-promo-img']);
        echo '</a>';
    }
?>
<div class="container">
    <div class="row">
        <div class="col text-center p-5">
            <?php if ($enlace){
                echo '<a href="'. $enlace .'" class=" bk--btn bk--btn__primary">Ver Promoción</a>';
            }else{
                echo '<a href="'. the_permalink() .'"class=" bk--btn bk--btn__primary">Ver Promoción</a>';
            }
            ?>
        </div>
    </div>
</div>
<?php endwhile; ?>
    
<?php if ( function_exists('b4st_pagination') ) { b4st_pagination(); } else if ( is_paged() ) { ?>
    <ul class="pagination">
    <li class="page-item older">
        <?php next_posts_link('<i class="fas fa-arrow-left"></i> ' . __('Previous', 'b4st')) ?></li>
<li class="page-item newer">
    <?php previous_posts_link(__('Next', 'b4st') . ' <i class="fas fa-arrow-right"></i>') ?></li>
</ul>
<?php } ?>

<?php
else :
get_template_part('./includes/loops/404');
endif;
?>
<?php get_template_part('includes/footer'); ?>
