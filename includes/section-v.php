<?php
/*
video
 */
if (is_front_page()):
    get_template_directory_uri();
    get_template_directory_uri();
    get_field('hm_titulo');
    get_field('hm_subtitulo');
    get_field('hm_texto');
    get_field('hm_img');
?>
<section class="pt-5 pb-5 bk-home--video text-white d-none d-md-block" style="background: url(<?php echo the_field('hm_img'); ?>) no-repeat center center;background-size: cover;">
    <div class="container pt-5 pb-5">
        <div class="row">
            <div class="col-lg-6">
                <div class="bk--title ">
                    <h2 class="text-center"><?php the_field('hm_titulo'); ?></h2>
                </div>
                <p class="text-center"><?php the_field('hm_subtitulo'); ?></p>
                    <p class="text-center"><?php the_field('hm_texto'); ?></p>
                <div class="text-center">
                    <a href="<?php the_field('hm_url_boton'); ?>" class="bk--btn bk--btn__primary" target="_blank">Descubre más ></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo do_shortcode('[wp-video-popup video="https://www.youtube.com/watch?v=nuR4oxxtSHU"]');?>
<?php endif; ?>

<?php
/*
video
 */
if (is_product()):
?>
<?php 
$videoUrl = get_field('enlace_video');
$videoImg = get_field('video_imagen');
$videoTitle = get_field('video_titulo');
$videoText = get_field('video_texto');

echo '<section class="pb-5 bk-home--video" style="background:url('.$videoImg['sizes'][ 'large' ].') no-repeat; background-size:cover;position:relative;">
<div class="bk-product--banner__filter"></div>    
<div class="container pb-5">
        <div class="row">
            <div class="col-lg-6 text-white">
                <div class="bk--title2 ">
                    <h2 class="text-center 2bk-video__title">'. $videoTitle .'</h2>
                    <p class="text-center">- Better Than Best -</p>
                </div>
                    <p class="text-center">'. $videoText .'</p>
                <div class="text-center">
                    <a href="#" class="wp-video-popup bk--btn bk--btn__primary">Descubre más ></a>
                </div> 
            </div>
            <!-- <div class="bk-home--video__img">
                <a href="#" class="wp-video-popup">
                    <img class="d-flex" src="'. $videoImg['sizes'][ 'large' ] .'" alt="'. $videoImg['alt'] .'">
                </a>
            </div>-->
        </div>
    </div>
</section>';
?>
<?php echo do_shortcode('[wp-video-popup video="https://youtu.be/'.$videoUrl.'"]');?>
<?php endif; ?>