<?php

?>
<?php $slider_args = array(
    'post_type'      => 'product',
    'posts_per_page' => 5
);
$slider = new WP_Query($slider_args);
if($slider->have_posts()):
//$count = $slider->found_posts;
$count = 5;
?>
<div class="container">
    <div class="row">
        <div class="col pt-5 pb-5">
            <div class="bk--title">
                <h2 class="text-center">Elige<span class="bk--title__i">tu Estilo</span></h2>
                <p class="text-center mb-0">- Better Than Best -</p>
            </div>
        </div>
    </div>
</div>
<section class="bk-loop--productos mb-5">
        <div class="bk-loop--productos__slider">

            <div class="owl-carousel owl-theme bk-product--slider">
            <?php while($slider->have_posts()): $slider->the_post(); ?>
                <a href="<?php the_permalink(); ?>" class="item">
                        <?php the_post_thumbnail( 'slider', array(
                            'class' => 'bk-loop--productos__slider-img img-fluid',
                            'alt' => get_the_title() ) ) ; ?>
                    <div class="bk-loop--productos__slider-txt">
                    <h3 class="text-uppercase bk-loop--productos__slider-title"><?php echo get_the_title(); ?></h3>
                    <?php $price = get_post_meta( get_the_ID(), '_price', true ); ?>
                    <h4 class="product-price-tickr">Desde <?php echo wc_price( $price ); ?></h4>
                    </div>
                </a>
            <?php endwhile; wp_reset_postdata();?>
            </div>

        </div>
</section>
<?php endif;  wp_reset_query(); ?>