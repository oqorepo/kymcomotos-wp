<?php $slider_args = array(
    'post_type'      => 'post',
    'posts_per_page' => 3
);
$slider = new WP_Query($slider_args);
if($slider->have_posts()):
$count = $slider->found_posts;
$count = 3;
?>
<section class=" bk-loop--novedades mb-5 d-none d-md-block">
    <div class="container">
        <div class=" pt-5 bk-loop--novedades__slider">
            <div class="pt-5 pb-4 bk--title bk--title__white bk-loop--novedades__slider-title">
                <h2 class="mb-3 d-md-inline">Últimas novedades</h2>
                <span class="ml-md-5">
                    <a href="<?php bloginfo('url'); ?>/novedades" class="bk--btn bk--btn__line">Ver Todas</a>  
                </span>
            </div>

            <div class="owl-carousel owl-theme bk-novedades--slider">

            <?php while($slider->have_posts()): $slider->the_post(); ?>
                    <a href="<?php the_permalink(); ?>">
                        <div class="carousel-item--contenedor">
                            <?php the_post_thumbnail('medium_size_w', array(
                                'class' => 'bk-loop--novedades__slider-img w-100',
                                'alt' => get_the_title() ) ) ; ?>
                        </div>
                        <div class="bk-loop--novedades__slider-txt">
                            <h3 class="text-uppercase"><?php echo get_the_title(); ?></h3>
                            <p class="pb-4"><?php echo get_the_date('d / m / y'); ?></p>
                        </div>
                    </a>
            <?php endwhile; ?>
            </div> <!--.carouse-inner-->
            
        </div>
    </div>
</section>
<?php endif;  wp_reset_postdata(); ?>