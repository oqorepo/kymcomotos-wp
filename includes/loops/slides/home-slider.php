<?php $slider_args = array(
    'post_type'      => 'home_slider',
    'posts_per_page' => 5,
    'post_status' => 'publish',
);
$slider = new WP_Query($slider_args);
if($slider->have_posts()):
$count = 5;
?>
<div class="container-fluid" style="padding:0px">
    <div id="banner-top" class="owl-carousel owl-theme">
        <?php while($slider->have_posts()): $slider->the_post(); ?>
            <div class="home-one-slide">
                <a href="<?php echo get_field('link')?>" alt=""><img src="<?php echo get_field('desktop')?>" class="d-none d-sm-block w-100" alt="diapositiva-home"></a>
                <a href="<?php echo get_field('link')?>" alt=""><img src="<?php echo get_field('mobile')?>" class="d-sm-none w-100" alt="diapositiva-home"></a>
            </div>
        <?php endwhile; wp_reset_postdata();?>
    </div>
</div>
<?php endif;  wp_reset_query(); ?>