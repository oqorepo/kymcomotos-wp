<?php
/**!
 * The Search Results Loop
 */
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <?php if(wp_is_mobile()) : ?>
  <div class="col-sm-4">
      <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card mt-5"); ?> >
          <a href="<?php the_permalink();?>" class="link">
              <img src="<?php the_post_thumbnail_url('medium'); ?>" class="w-100">
              <div class="text-center">
                  <h2><?php echo the_title();?></h2>
                  <a class="info bk--btn bk--btn__primary bk--btn__small mt-3 p-2" href="<?php the_permalink();?>">Ver más ></a>
              </div>
          </a>
      </article>
  </div>
  <?php else : ?>
  <div class="col-sm-3">
    <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card mt-5"); ?> >
        <!-- <header class="bk-loop-card--header">
            <?php the_post_thumbnail(); ?>
        </header>
        <div class="bk-loop-card--content">
            <h2>
                <a href="<?php the_permalink(); ?>"> <?php the_title()?> </a>
            </h2>
        </div> -->

        <div class="hovereffect">
        <?php if( have_rows('cn-datos') ): while( have_rows('cn-datos') ): the_row();
        $image = get_sub_field('cn-img');
          if ( !empty($image) ) {
              echo '<img src="'. $image['url'] .'" alt="">';
          }
          else {
            echo '<img src="' . get_bloginfo( 'stylesheet_directory' ). '/assets/img/rinomotos.png" />';
          }
          ?>
          <?php endwhile; endif; ?>
            <a href="<?php the_permalink();?>" class="link">
                <div class="overlay">
                    <h2><?php echo the_title();?></h2>
                    <a class="info bk--btn bk--btn__primary bk--btn__small" href="<?php the_permalink();?>">Ver más ></a>
                </div>
            </a>
        </div>
    </article>
  </div>
<?php endif; ?>
<?php endwhile; else: ?>
  <div class="alert alert-warning">
    <i class="fas fa-exclamation-triangle"></i> <?php _e('¡Lo sentimos! No se encontraron resultados para tu busqueda de concesionario', 'b4st'); ?>
  </div>
<?php endif; ?>
