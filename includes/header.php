<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
  <head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PX9KTCR');</script>
    <!-- End Google Tag Manager -->
    
    <meta name="description" content="<?php if ( is_single() ) {
        single_post_title('', true);
      } else {
        bloginfo('name'); echo " - "; bloginfo('description');
      }
    ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PX9KTCR"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <?php 
  if (is_woocommerce()){
    get_template_part('includes/slide-c'); 
  }
  ?>
  <main class="main">
    <div class="cd-loader">
      <small>...</small>
      <div class="cd-loader__grid">
        <div class="cd-loader__item"></div>
      </div>
    </div>

    <nav class="bk-menu-social">
      <ul class="bk-menu-social__content">
        <li class="bk-menu-social__item">
          <a href="https://www.facebook.com/kymcocl/" target="_blank">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-menu-facebook.png" alt="Visita nuestro Facebook">
          </a>
        </li>
        <li class="bk-menu-social__item">
          <a href="https://www.instagram.com/kymcochile/" target="_blank">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-menu-instagram.png" alt="Visita nuestro instagram">
          </a>
        </li>
        <li class="bk-menu-social__item">
          <a href="https://www.youtube.com/channel/UCljA1LDycQD3U09ZOzl06jQ" target="_blank">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-menu-youtube.png" alt="Visita nuestro canal de youtube">
          </a>
        </li>
      </ul>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-light bk-navbar">
      <div class="container bk-navbar--container">

        <a class="navbar-brand bk-navbar--logo" href="<?php echo esc_url( home_url('/') ); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/bg-footer.jpg" alt="" class="bk-navbar--logo__img"></a>

        <button class="hamburger hamburger--emphatic navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdown" aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>

        <div class="collapse navbar-collapse bk-navbar--collapse" id="navbarDropdown">

          <?php
            wp_nav_menu( array(
              'theme_location'  => 'navbar',
              'container'       => false,
              'menu_class'      => '',
              'fallback_cb'     => '__return_false',
              'items_wrap'      => '<ul id="%1$s" class="navbar-nav bk-navbar__nav %2$s">%3$s</ul>',
              'depth'           => 2,
              'walker'          => new b4st_walker_nav_menu()
            ) );
          ?>

        </div>
      </div>
    </nav>
    <hr class="d-md-none">
    <div class="container-fluid social-menu-bar d-md-none" style="background:#ffffff">
      <div class="container">
        <div class="row pb-1 pt-1">
          <div class="col-4">
            <a href="https://www.facebook.com/kymcocl/" target="_blank" class="d-block text-center">
              <i class="fab fa-facebook-f"></i>
              <!-- <img src="<?php bloginfo('template_directory'); ?>/assets/img/fb.jpg" alt="Visita nuestro Facebook" width="40"> -->
            </a>
          </div>
          <div class="col-4">
            <a href="https://www.instagram.com/kymcochile/" target="_blank" class="d-block text-center">
              <i class="fab fa-instagram"></i>
              <!-- <img src="<?php bloginfo('template_directory'); ?>/assets/img/ins.jpg" alt="Visita nuestro instagram" width="40"> -->
            </a>
          </div>
          <div class="col-4">
            <a href="https://www.youtube.com/channel/UCljA1LDycQD3U09ZOzl06jQ" target="_blank" class="d-block text-center">
              <i class="fab fa-youtube"></i>
              <!-- <img src="<?php bloginfo('template_directory'); ?>/assets/img/yt.jpg" alt="Visita nuestro canal de youtube" width="40"> -->
            </a>
          </div>
        </div>
      </div>
    </div>
    <?php if( is_front_page() ):?>
      <?php get_template_part('includes/loops/slides/home-slider');?>
    <?php elseif( is_home() ):?>
      <img src="<?php bloginfo('template_directory')?>/assets/img/novedades-bg.jpg" alt="" style="width:100%;">
    <?php elseif( is_archive('promociones') || is_singular('promociones') ):?>
    <?php elseif( is_product()):?>
    <?php elseif( is_shop()):?>
    <?php elseif( is_page_template('template-noheader.php')):?>
      <img src="<?php bloginfo('template_directory')?>/assets/img/landing-hero.png" alt="" style="width:100%;">
    <?php elseif( is_page('suzuki-army-chile')):?>
    <?php elseif(is_singular('experience') ):?>
    <?php elseif(is_singular('army') ):?>
    <?php else:?>
      <img src="<?php bloginfo('template_directory')?>/assets/img/bk-header-default.jpg" alt="" style="width:100%;">
    <?php endif?>