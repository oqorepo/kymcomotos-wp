<?php
/*
sec concesionarios
 */
?>
<section class="container-fluid h-100 mt-5 bk-home--search d-none d-md-block">
    <div class="row h-100 flex-column justify-content-center align-items-center bk-home--search__content">
        <div class="bk--title bk--title__white">
            <div class="text-center bk-home--search__icons">
                <i class="fas fa-map-marker-alt"></i>
            </div>
            <h2 class="text-center">Busca tu concesionario más cercano</h2>
            <p class="text-center" style="color:#ffffff;">- Better Than Best -</p>
        </div>
        <form class="pt-2 pb-2 bk-home--search__form" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
            <div class="input-group mb-3">
                <input class="form-control bk-search--input" type="text" value="<?php echo get_search_query(); ?>" placeholder="Ingresar comuna o nombre del Concesionario" name="s" id="s">
                <div class="input-group-append">
                    <button type="submit" id="searchsubmit" value="<?php esc_attr_x('Search', 'b4st') ?>" class="bk--btn bk--btn__primary bk-search--input__btn" disabled style="border-radius:5px !important;">
                        <i class="fas fa-search"></i>
                    </button>
                </div>
            </div>
          </form>
        <!-- <div class="bk--title__white">
            <p class="text-center"><i class="fas fa-crosshairs" style="font-size:1.3em;"></i> Buscar en los alrededores</p>
        </div> -->
    </div>
</section>