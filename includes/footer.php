    </main>
    <?php if( is_page_template('template-noheader.php')):?>
      <img src="<?php bloginfo('template_directory')?>/assets/img/landing-footer.png" alt="" style="width:100%;">
    <?php endif?>
    <footer class="bk-footer">
        <?php if (is_front_page() || is_page('contacto-ok')){
            $variable = get_template_directory_uri();
            echo '<img src="'. $variable .'/assets/img/trans-11.jpg" class="w-100">';
        }
        ?>
        <div class="container pt-5 pb-5">
            <div class="row">
                <div class="col">
                    <div class="bk--title">
                        <h2 class="text-center">Suscríbete a nuestro<span class="bk--title__i"> newsletter</span></h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-end">
                <div class="col-sm mt-2">
                    <div class="text-center">  
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg-footer2.png" alt="kymco" class="bk-footer__img">
                    </div>
                </div>
                <div class="col-sm mt-2">
                    <?php 
                        // Formulario local echo do_shortcode('[contact-form-7 id="1021" title="mailing"]');
                        // Formulario en producción
                        echo do_shortcode('[contact-form-7 id="1217" title="Newsletter" html_id="formulario_newsletter"]');
                     ?>
                    
                    <div class="input-group">
                        <input type="email" class="form-control bk-search--input bk-mailing-visible--input" placeholder="Ingresa tu email" aria-label="Ingresa tu email" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <a class="bk--btn bk--btn__primary bk-search--input__btn" id="bkSbmitFooterBtn" style="border-radius:5px !important;"><i class="fas fa-paper-plane" style="transform: skew(12deg);"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm mt-2">
                    <div class="bk-footer--logos text-center">
                        <a href="http://www.dercomotos.cl/"><img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-derco.png" alt=""></a>
                    </div>  
                </div>
            </div>
            <div class="row pt-3">
                <div class="col-sm">
                    <h4 class="text-center">
                        <i class="fas fa-phone"></i> <a href="tel:6006873737"> 600 687 37 37</a>
                        <?php if ( wp_is_mobile() ) : ?>
                            <br><i class="fas fa-phone"></i> <a href="tel:227875943">2 2787 5943</a>
                        <?php else : ?>
                            &nbsp;&nbsp;&nbsp; <a href="tel:227875943">2 2787 5943</a>
                        <?php endif; ?>
                    </h4>
                </div>
            </div>
        </div>

<!--         <?php if(is_active_sidebar('footer-widget-area')): ?>
        <div class="row border-bottom pt-5 pb-4" id="footer" role="navigation">
          <?php dynamic_sidebar('footer-widget-area'); ?>
        </div>
        <?php endif; ?> -->
        
        <div class="container-fluid pt-4 pb-3 bk-footer--menu">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <?php wp_nav_menu( array( 
                            'theme_location' => 'footer-menu',
                            'menu_class'    => 'd-flex justify-content-between'
        
                        ) ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid pt-2 pb-2 bk-footer--copy">
            <div class="row">
                <div class="col ">
                    <div class="text-center pt-2 pb-2">
                        <small class="text-center"><a href="<?php echo home_url('/'); ?>" target="_blank" class="bk-link--white">Todos los derechos reservados &copy; <?php //echo date('Y'); ?> <?php bloginfo('name'); ?></a></small>
                    </div>
                </div>
            </div>
        </div>
        
    </footer>
    <?php wp_footer(); ?>
    <!-- Modal Servicio Técnico-->
    <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="contactModalLabel">Servicio Técnico</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body container">
                        <?php echo do_shortcode( '[contact-form-7 id="6" title="Contacto"]' ); ?>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div><!-- Modal Servicio Técnico-->

        <!-- Modal Servicio Técnico-->
        <div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="serviceModalLabel">Contacto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body container">
                    <p>¿QUIERES SABER ALGO MÁS? ¡CONTÁCTANOS! Si tienes dudas o consultas, rellena nuestro formulario y te responderemos a la brevedad.</p>
                    <?php echo do_shortcode( '[contact-form-7 id="6" title="Contacto"]' ); ?>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div><!-- Modal Servicio Técnico-->
    </main>
  </body>
</html>