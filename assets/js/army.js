$(document).ready(function () {
    $('#army-owl').owlCarousel({
		loop:true,
		nav:false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			}
		}
    });
    $('.experience-owl').owlCarousel({
		loop:false,
        nav:false,
        margin: 5,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			}
		}
	});
	$('#video_post').YTPlayer({
        fitToBackground: true,
        videoId: ''+my_data.video_url_post+'',
		pauseOnScroll: false,
        height: '100%',
        width: '100%',
        anchor: 'center,center',
        playerVars: {
            modestbranding: 0,
            autoplay: 1,
            controls: 0,
            showinfo: 0,
            wmode: 'transparent',
            branding: 0,
            rel: 0,
            autohide: 0,
            origin: window.location.origin
        }
	}); 
	console.log(my_data.video_url_post);
});