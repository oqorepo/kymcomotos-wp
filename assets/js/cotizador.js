/*
------------------------------------------------------------------
  Funciones para slide cotización Kymco motos
  Creado      por: Gustavo Rivero
------------------------------------------------------------------
*/

var btn_modelo = $('#formulario_cotizar').parent().find('input[name="moto_modelo"]').val();

function financiamientoRadio(v, h) {
  console.log(v);

  if (v == 'cotizar') {
    // $('.autofin-container').addClass('hidden');
    $('#formluario_cotizar input[name="' + v + '"]').prop('checked');
    $('#formulario_cotizar #boton_enviar').text(' Cotizar ' + btn_modelo + ' > ');
    if (h) {
      $('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').attr('checked', true).prop('disabled', false);
      $('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').attr('checked', false).prop('disabled', true);
    }
  } else if (v == 'evaluar') {
    // $('.autofin-container').removeClass('hidden');
    $('#formulario_cotizar #boton_enviar').text(' Cotizar ' + btn_modelo + ' y Solicitar evaluación > ');
    if (h) {
      $('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').attr('checked', true).prop('disabled', false);
      $('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').attr('checked', false).prop('disabled', true);
    }
  }
};


$(document).ready(function () {

  /* 
  ------------------------------------------------------------------
    Cotizador lateral
  ------------------------------------------------------------------
  */
  // Cotizador lateral trigger
  $('.cd-nav-trigger').on('click', function () {
    $('.cd-nav').toggleClass('loaded');
    $('.main').toggleClass('navigation-is-open');
    $('.close-cd-nav-trigger').addClass('is-active');
    $('html, body').animate({
      scrollTop: 0
    }, '300');
  });
  // Close cotizador lateral trigger
  $('.close-cd-nav-trigger').on('click', function () {
    $('.cd-nav').toggleClass('loaded');
    $('.main').toggleClass('navigation-is-open');
    $('.main').show('fast');
    $(this).removeClass('is-active');
    //$('#boton_enviar').removeClass('d-none');
  });
  // Reset Formulario y Close cotizador lateral trigger
  $('#boton_reset_form').on('click', function () {
    $('.cd-nav').toggleClass('loaded');
    $('.main').toggleClass('navigation-is-open');
    $('.main').show('fast');
    $('#formulario_cotizar').fadeIn('slow');
    $('.bk-cot--card__content--cuote').fadeIn();
    $('.bk-cot--card__content').fadeIn();
    $('#gracias-form-cotizar').fadeOut();
    $('.wpcf7-response-output').fadeOut();
    $('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').attr('checked', true);
    $('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').attr('checked', false);
    $('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').prop('disabled', false);
    $('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').prop('disabled', false);
    $(this).removeClass('is-active');
    //$('#boton_enviar').removeClass('d-none');
  });

  /*
  ------------------------------------------------------------------
    Concesionarios en cotización
  ------------------------------------------------------------------
  */

  //me trae las comunas de la zona seleccionada
  
  $("#bk_zona").on('change', function () {
    var region = $(this).val(),
    code = {},
    opciones = '';
    $('#bk_comuna').html('<option>cargando...</option>');
    $('#bk_concesionario').html('<option value="">Selecione una comuna primero...</option>');
    if (parseInt(region) != "0") {
      opciones += '<option value="" selected="selected">Seleccione una Comuna</option>'
      $.each(my_data.ces_data, function (k, v) {
        console.log(v);
        if (region == v.region) {
          opciones += '<option value="' + v.comuna + '">' + v.comuna + '</option>';
        }
      });
      $('#bk_comuna').html(opciones);
    }
    //console.log(v.comuna);
    
    //remueve duplicados de la iterecion anterior
    $("#bk_comuna > option").each(function () {
      if (code[this.text]) {
        $(this).remove();
      } else {
        code[this.text] = this.value;
      }
    });
  });

  //me trae los CES de la comuna 
  $("#bk_comuna").on('change', function () {
    $('.caja_opcion_financiar').fadeOut();
    $('.caja_nota_concesionario').fadeOut();
    var comuna = $(this).val(),
      opciones = '';
    if (comuna == '') {
      $('#bk_concesionario').html('<option></option>');
    } else {
      $('#bk_concesionario').html('<option>cargando...</option>');
      $('.caja_nota_concesionario').fadeOut();
    }
    if (parseInt(comuna) != "") {
      opciones += '<option value="" selected="selected">Seleccione un Concesionario</option>';
      $.each(my_data.ces_data, function (k, v) {
        if (comuna == v.comuna) {
          console.log("vamos bien");
          opciones += '<option value="' + v.nombreCes + '" data-ces-id-plat="' + v.cesIdPlat + '" data-autofin="' + v.cesAutofin + '" data-mail="' + v.emailCes + '" data-mail-dos="' + v.emailCesDos + '" data-mail-tres="' + v.emailCesTres + '" >' + v.nombreCes + '</option>';
        }
      });
      $('#bk_concesionario').html(opciones);
    }
    $('#formulario_cotizar input[name="ces_comuna"]').val(comuna);
    $('#formulario_cotizar input[name="ces_comunas"]').val(comuna);
  });

  $("#bk_concesionario").on('change', function () {
    $('.caja_opcion_financiar').fadeOut();
    $('.caja_nota_concesionario').fadeOut();
    var ces_nombre = $(this).find(':selected').val(),
      ces_nombre = $(this).find(':selected').val(),
      ces_id_plat = $(this).find(':selected').data("ces-id-plat"),
      ces_email = $(this).find(':selected').data("mail"),
      ces_email_dos = $(this).find(':selected').data("mail-dos"),
      ces_email_tres = $(this).find(':selected').data("mail-tres"),
      ces_autofin = $(this).find(':selected').data("autofin"),
      zon_nombre = $('#bk_zona option:selected').text(),
      moto_modelo = $('#formulario_cotizar input[name="moto_modelo"]').val(),
      moto_autofin = $('#formulario_cotizar input[name="moto_autofin"]').val();
    $('#formulario_cotizar input[name="id_ces_plat"]').val(ces_id_plat);
    $('#formulario_cotizar input[name="ces_autofin"]').val(ces_autofin);
    $('#formulario_cotizar input[name="ces_email"]').val(ces_email);
    $('#formulario_cotizar input[name="ces_email_dos"]').val(ces_email_dos);
    $('#formulario_cotizar input[name="ces_email_tres"]').val(ces_email_tres);
    $('#formulario_cotizar input[name="ces_nombre"]').val(ces_nombre);
    $('#formulario_cotizar input[name="zon_nombre"]').val(zon_nombre);

    if (moto_autofin != '' && ces_autofin != '') {
      $('.caja_opcion_financiar').fadeIn();
      $('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').attr('checked', true);
      $('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').attr('checked', false);
    } else {
      $('.caja_nota_concesionario').fadeIn();
      $('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').attr('checked', true);
      //$('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').attr('checked', false);
    }
  });

  $('#formulario_cotizar input:radio[name=financiamiento]').change(function () {
    if (this.value == 'cotizar') {
      $('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').attr('checked', true);
      $('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').attr('checked', false);
      $('#formulario_cotizar #boton_enviar').text(' Cotizar ' + btn_modelo + ' > ');
    } else if (this.value == 'evaluar') {
      $('#formulario_cotizar input:radio[name="financiamiento"][value="evaluar"]').attr('checked', true);
      $('#formulario_cotizar input:radio[name="financiamiento"][value="cotizar"]').attr('checked', false);
      $('#formulario_cotizar #boton_enviar').text(' Cotizar ' + btn_modelo + ' y Solicitar evaluación > ');
    }
  });

  // ----------------------------------------------------------------------
  // -------------------- Formulario Cotizar Moto --------------------
  // ----------------------------------------------------------------------

  var frm_destino = $('#formulario_original').attr('action');
  var frm_clases = $('#formulario_original').attr('class');
  $('#formulario_cotizar').attr('action', frm_destino);
  $('#formulario_cotizar').attr('class', frm_clases);
  $('#caja_formulario_original').remove();

  // Bloquea envio de form
  // $('#formulario_cotizar #boton_enviar').prop('disabled', true);

  var utm_source = sbjs.get.current.src,
    utm_medium = sbjs.get.current.mdm,
    utm_campaign = sbjs.get.current.cmp,
    utm_content = sbjs.get.current.cnt,
    utm_term = sbjs.get.current.trm,
    utm_code = sbjs.get.promo.code;


  if ($('#formulario_cotizar input[name="utm_source"]').length > 0) {
    $('#formulario_cotizar input[name="utm_source"]').val(utm_source);
  }
  if ($('#formulario_cotizar input[name="utm_medium"]').length > 0) {
    $('#formulario_cotizar input[name="utm_medium"]').val(utm_medium);
  }
  if ($('#formulario_cotizar input[name="utm_campaign"]').length > 0) {
    $('#formulario_cotizar input[name="utm_campaign"]').val(utm_campaign);
  }
  if ($('#formulario_cotizar input[name="utm_content"]').length > 0) {
    $('#formulario_cotizar input[name="utm_content"]').val(utm_content);
  }
  if ($('#formulario_cotizar input[name="utm_term"]').length > 0) {
    $('#formulario_cotizar input[name="utm_term"]').val(utm_term);
  }
  if ($('#formulario_cotizar input[name="utm_code"]').length > 0) {
    $('#formulario_cotizar input[name="utm_code"]').val(utm_code);
  }

  var currentdate = new Date();
  var fecha = currentdate.getDate() + "-" +
    (currentdate.getMonth() + 1) + "-" +
    currentdate.getFullYear() + " @ " +
    currentdate.getHours() + ":" +
    currentdate.getMinutes();
  $('#formulario_cotizar input[name="cot_fecha"]').val(fecha);

  //var val_nor_cp = $('#formulario_cotizar input[name="moto_precio"]').val();
  $('#formulario_cotizar input[name="moto_precio_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_precio"]').val()));
  $('#formulario_cotizar input[name="moto_bono_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_bono"]').val()));
  $('#formulario_cotizar input[name="moto_precio_final_p"]').val(agregarPuntos($('#formulario_cotizar input[name="moto_precio_final"]').val()));

  // Arregla el campo de RUT
  $('#formulario_cotizar .Rut').Rut({
    on_error: function () {},
    format_on: 'keyup'
  });

  $('#contactoTel').rules("add", {
    required: true,
    number: true,
    minlength: 9,
    maxlength: 9,
    messages: {
      required: "Teléfono requerido",
      minlength: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos."),
      maxlength: jQuery.validator.format("Por favor ingresa un teléfono de 9 digitos."),
    }
  });
  //Mensajes Personalizados
  jQuery.extend(jQuery.validator.messages, {
    required: "Este campo es obligatorio",
    email: "Por favor ingresa un correo válido.",
    number: "Please sólo numeros",
    lettersonly: "Por favor ingresa sólo letras.",
    digits: "Por favor ingresa sólo números."
  });

  $('#formulario_cotizar').on('keyup keypress', function (e) {
    if ($(this).valid()) {
      $('#boton_enviar').prop('disabled', false);
    } else {
      $('#boton_enviar').prop('disabled', true);
    }
  });
  
  // Botón enviar
  $('#boton_enviar').on('click', function (event) {
    $('html, body').animate({
      scrollTop: 0
    }, '300');
    $('#formulario_cotizar').fadeOut('slow');
    $('.bk-cot--card').fadeOut();
    $('#cargador-cotizar').fadeIn();
  });


  //test
  document.addEventListener( 'wpcf7mailsent', function( event ) {
    //alert( "wpcf7mailsent!" );
  }, false );
  $("#wpcf7-f115-o1").on('wpcf7:submit', function (event) { 
    console.log('Mail:submit')
  });
  $("#wpcf7-f115-o1").on('wpcf7:mailsent', function (event) { 
    console.log('Mail:sent')
  });
  $("#wpcf7-f115-o1").on('wpcf7submit', function (event) { 
    console.log('Mailsubmit')
  });
  $("#wpcf7-f115-o1").on('wpcf7mailsent', function (event) { 
    console.log('Mailsent')
  });
  $("#wpcf7-f115-o1").on('submit', function (event) { 
    console.log('submit')
  });
  $("#wpcf7-f115-o1").on('mailsent', function (event) { 
    console.log('sent')
  });

  // Proceso enviador
  $("#wpcf7-f115-o1").on("wpcf7:mailsent", function (event) { //CAMBIAR SUBMIT POR MAILSENT ANTES DE SUBIR
    console.log("mailsent");
    //alert( "wpcf7mailsent!" );

    var $estadoLB = $('#formulario_cotizar input[name=financiamiento]:checked').val(),
      finan = $('#formulario_cotizar input[name=financiamiento]:checked').val();
    //setCookie(sesionUnica, 1);
    window.localStorage.setItem('sesionUnica', 1);
    //window.sesionUnica = setCookie(sesionUnica, 1);
    if ($estadoLB === 'evaluar') {
      var metric_cat = $('#formulario_cotizar input[name="moto_cat"]').val();
      var lb_name = $('#formulario_cotizar input[name="contacto_nombre"]').val();
      var lb_apellido = $('#formulario_cotizar input[name="contacto_apellido"]').val();
      var lb_cesname = $('#formulario_cotizar input[name="ces_nombre"]').val();
      var lb_pmodel = $('#formulario_cotizar input[name="moto_autofin"]').val();
      var m_precion = $('#formulario_cotizar input[name="moto_precio"]').val();
      var precio_bonof = $('#formulario_cotizar input[name="value_bonoautofin"]').val();
      if( $('#bonoautofinyes').prop('checked') ) {
        var m_preciof = $('#formulario_cotizar input[name="value_conbonos"]').val();
        var lb_value_total = $('#formulario_cotizar input[name="value_conbonos"]').val();
      } else {
        var m_preciof = $('#formulario_cotizar input[name="moto_precio_final"]').val();
        var lb_value_total = $('#formulario_cotizar input[name="moto_precio_final"]').val();
      };
      var m_bono = $('#formulario_cotizar input[name="moto_bono"]').val();
      var lb_autofin_model = $('#formulario_cotizar input[name="moto_ano"]').val();
      var lb_brand_vehicle = $('#formulario_cotizar input[name="moto_marca"]').val();
      var lb_rut = $('#formulario_cotizar input[name="contacto_rut"]').val().replace('.', '').replace('.', '');
      var lb_email = $('#formulario_cotizar input[name="contacto_email"]').val();
      var lb_fono = $('#formulario_cotizar input[name="contactoTel"]').val();
      var lb_ces = $('#formulario_cotizar input[name="ces_autofin"]').val();
      var lb_fotom = $('#formulario_cotizar input[name="moto_thumb"]').val();
      var lb_Autofin = 'https://www.trinidadautofin.com/SpiderDercoMoto/index.html?idTercero=2&plazo=24&modelo=' + lb_pmodel + '&marca=' + lb_brand_vehicle + '&valorVehiculo=' + lb_value_total + '&ano=' + lb_autofin_model + '&r=' + lb_rut + '&t=' + lb_fono + '&d=' + lb_ces + '&c=9&e=' + lb_email;

       //$('.caja_opcion_financiar').fadeOut();
      //$('.caja_nota_concesionario').fadeOut();
      //console.log("Evaluación");
      $("#autofinIframe").attr('src', lb_Autofin);
      //$('#formulario_cotizar').fadeOut('slow');
      //$('.bk-cot--card__content--cuote').fadeOut();
      //$('.bk-cot--card__content').fadeOut();
      //$('.bk-cot--card__img').fadeOut();

      //almacenamos las variables en el localStorage, ante de redireccionar y para usar la data en la pagina de contacto exitoso.
      window.localStorage.setItem('metric_cat', metric_cat);
      window.localStorage.setItem('nombre', lb_name);
      window.localStorage.setItem('apellido', lb_apellido);
      window.localStorage.setItem('cesname', lb_cesname);
      window.localStorage.setItem('modelo', lb_pmodel);
      window.localStorage.setItem('precion', m_precion);
      window.localStorage.setItem('preciof', m_preciof);
      window.localStorage.setItem('bonoautofin', precio_bonof);
      window.localStorage.setItem('m_bono', m_bono);
      window.localStorage.setItem('fotom', lb_fotom);
      window.localStorage.setItem('fin_url', lb_Autofin);
      window.localStorage.setItem('finan', finan);
      window.localStorage.setItem('motoCotizada', lb_pmodel);
      var price = $('#motoprice').text();
      var cae = $('#motocae').text();
      window.localStorage.setItem('cae', cae);
      window.localStorage.setItem('price', price);
    }

    if ($estadoLB == 'cotizar') {
      console.log("Cotización");

      //recogemos las variables para la página de éxito
      var metric_cat = $('#formulario_cotizar input[name="moto_cat"]').val();
      var lb_name = $('#formulario_cotizar input[name="contacto_nombre"]').val(),
        lb_apellido = $('#formulario_cotizar input[name="contacto_apellido"]').val(),
        lb_cesname = $('#formulario_cotizar input[name="ces_nombre"]').val(),
        lb_pmodel = $('#formulario_cotizar input[name="moto_modelo"]').val(),
        m_precion = $('#formulario_cotizar input[name="moto_precio"]').val(),
        precio_bonof = $('#formulario_cotizar input[name="value_bonoautofin"]').val();
        m_bono = $('#formulario_cotizar input[name="moto_bono"]').val(),
        lb_fotom = $('#formulario_cotizar input[name="moto_thumb"]').val(),
        use_autofin = $('#formulario_cotizar input[name="ces_autofin"]').val();
        if( $('#bonoautofinyes').prop('checked') ) {
          var m_preciof = $('#formulario_cotizar input[name="value_conbonos"]').val();
        } else {
          var m_preciof = $('#formulario_cotizar input[name="moto_precio_final"]').val();
        }

      //almacenamos las variables en el localStorage, ante de redireccionar y para usar la data en la pagina de contacto exitoso.
      window.localStorage.setItem('metric_cat', metric_cat);
      window.localStorage.setItem('nombre', lb_name);
      window.localStorage.setItem('apellido', lb_apellido);
      window.localStorage.setItem('cesname', lb_cesname);
      window.localStorage.setItem('modelo', lb_pmodel);
      window.localStorage.setItem('precion', m_precion);
      window.localStorage.setItem('preciof', m_preciof);
      window.localStorage.setItem('m_bono', m_bono);
      window.localStorage.setItem('bonoautofin', precio_bonof);
      window.localStorage.setItem('fotom', lb_fotom);
      window.localStorage.setItem('autofin', use_autofin);
      window.localStorage.setItem('finan', finan);
      window.localStorage.setItem('motoCotizada', lb_pmodel);
      var price = $('#motoprice').text();
      var cae = $('#motocae').text();
      window.localStorage.setItem('cae', cae);
      window.localStorage.setItem('price', price);
    }

    window.location.href = my_data.templateUrl + "/contacto-ok";
  });

  //redireccionamos a la página de éxito
  /*
  $("#wpcf7-f115-o1").on('wpcf7:mailsent', function (event) {
    alert('mailsent');
  });

  $("#wpcf7-f115-o1").on('wpcf7:mailfailed', function (event) {
    alert("mailfailed")
  });

  $("#wpcf7-f115-o1").on('wpcf7:invalid', function () {
    alert("invalid")
  });
  $("#wpcf7-f115-o1").on('wpcf7:submit', function () {
    alert("wpcf7submit")
  });
  */

  $.validator.addMethod("ces_zona", function (value, element) {
    if ($('#bk_zona').val() == 0) {
      return false;
    } else {
      return true;
    }
  }, "Región es obligatorio");
  // Validador de RUT
  $.validator.addMethod("Rut", function (value, element) {
    return this.optional(element) || $.Rut.validar(value);
  }, "Este campo debe ser un rut valido.");
  // Validación de sólo letras y espacio
  jQuery.validator.addMethod("lettersonly", function (value, element) {
    return this.optional(element) || /^[a-z\s]+$/i.test(value);
  }, "Por favor ingresa sólo letras.");
});
