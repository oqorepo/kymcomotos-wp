<?php get_template_part('includes/header'); 
$today = date("Ymd");?>
<img src="<?php bloginfo('template_directory')?>/assets/img/bk-header-default.jpg" alt="" style="width:100%;">
<section class="container mt-5">
    <div class="row">
        <h2 class=" text-center w-100 bk--title">
        experience 
            <?php 
              $terms = get_the_terms( $post->ID , 'eventype' );
              foreach ( $terms as $term ) {
                echo $term->name;
              }
            ?>
            </h2>
            <p class="text-center w-100">- Better Than Best -</p>
    </div>
    <div class="row mb-5">


    <?php if(have_posts()) : ?>
        <?php while(have_posts()) : the_post(); 
        $ex_img_event = get_field('ex_imagen_evento');
        $ex_img_event_url = $ex_img_event['sizes']['medium'];
        ?>
    
        <div class="col-sm-3">
            <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card mt-3"); ?> >
                <!-- <header class="bk-loop-card--header">
                    <?php the_post_thumbnail(); ?>
                </header>
                <div class="bk-loop-card--content">
                    <h2>
                        <a href="<?php the_permalink(); ?>"> <?php the_title()?> </a>
                    </h2>
                </div> -->

                <div class="hovereffect">
                <img src="<?php echo $ex_img_event_url; ?>" class="img">
                    <a href="<?php the_permalink();?>" class="link">
                        <div class="overlay">
                            <h2><?php echo the_title();?></h2>
                            <a class="info bk--btn bk--btn__primary bk--btn__small" href="<?php the_permalink();?>">Ver más ></a>
                        </div>
                    </a>
                </div>
            </article>
        </div>
        
        <?php endwhile; ?>
    
        <?php if ( function_exists('b4st_pagination') ) {
            echo'
            <div class="container">
                <div class="row justify-content-center pt-5">
            ';
            b4st_pagination(); 
            echo'
                </div>
            </div>
            ';
        } else if ( is_paged() ) { ?>
            <div class="container p-5">
                <div class="row text-center">
                    <ul class="pagination">
                        <li class="page-item older">
                            <?php next_posts_link('<i class="fas fa-arrow-left"></i> ' . __('Previous', 'b4st')) ?>
                        </li>
                        <li class="page-item newer">
                            <?php previous_posts_link(__('Next', 'b4st') . ' <i class="fas fa-arrow-right"></i>') ?>
                        </li>
                    </ul>
                </div>
            </div>   
        <?php } ?>
    
    <?php
    else :
    get_template_part('./includes/loops/404');
    endif;
    ?>

    </div>

</section><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
