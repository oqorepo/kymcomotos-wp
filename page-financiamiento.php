<?php get_template_part('includes/header'); ?>

<section class="container mt-5">
  <div class="row">
    <div class="col-sm">
      <div id="content" role="main">
        <?php get_template_part('includes/loops/page-content'); ?>
      </div><!-- /#content -->
    </div>

  </div><!-- /.row -->
</section><!-- /.container -->

<section class="container-fluid bk-financiamiento-bg">
    <div class="container p-5 mb-5">
        <div class="row">
            <div class="col-sm-12 text-center">
                <img class="p-5" src="<?php bloginfo('template_directory'); ?>/assets/img/bk-autofin-logo.png" alt="">
                <p>Si te decidiste por comprar una moto kymco, entonces atrévete con Autofin. <br> Pagar un pie desde un 30% y cancela tus cuotas en un período desde 6 a 36 meses.</p>
            </div>
        </div>
        <ul class="row text-center">
            <li class="col-sm">
                <p><b>TASA INTERÉS ANUAL</b></p>
                <h4 class="bk--title">FIJA</h4>
            </li>
            <li class="col-sm">
                <p><b>PLAZO HASTA </b></p>
                <h4 class="bk--title">36 <small>(MESES)</small></h4>
            </li>
            <li class="col-sm">
                <p><b>MONTO DE PIE DESDE</b></p>
                <h4 class="bk--title">30%</h4>
            </li>
            <li class="col-sm">
                <p><b>PRIMERA CUOTA HASTA </b></p>
                <h4 class="bk--title">60 <small>(DÍAS)</small></h4>
            </li>
        </ul>
    </div>
</section>

<section class="bk-financiamiento container mb-5">
    <div class="row">
        <div class="col-sm">
            <ul class="pills-navigation nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-seleccionar-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-seleccionar" aria-selected="true">
                        <h4 class="d-inline">01 </h4>Elige tu moto
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-simulacion-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-simulacion" aria-selected="false">
                        <h4 class="d-inline">02 </h4>Ingresa tus datos
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-autofin-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-autofin" aria-selected="false">
                        <h4 class="d-inline">03 </h4>Simulación de Credito
                    </a>
                </li>
            </ul>

            <!-- contenido 1 -->
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-seleccionar" role="tabpanel" aria-labelledby="pills-seleccionar-tab">
                    <div class="container">
                        <div class="" style="text-align:center;margin:20px auto;">
                            <h3>Selecciona un rango de precios:</h3>
                            <input type="text" id="amount" style="border:0;color:#f6931f;font-weight:bold;width:100%;text-align:center;" />
                        </div>
                        <div class="" style="text-align:center;margin:20px auto 40px;">
                            <div id="slider-range"></div>
                        </div>
                        
                        <ul class="products columns-4 row" id="products">
<?php
                    global $product;

                    $producto_args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 100
                    );
                    $producto = new WP_Query($producto_args);
                    if ($producto->have_posts()) :

                        while ($producto->have_posts()) : $producto->the_post();
                            $autofinValor = $product->get_attribute('autofin');
                            if (!empty($autofinValor)) {

                                 if ($product->is_type('simple')) {
                                    $precio_normal = (int)get_post_meta(get_the_ID(), '_regular_price', true);;
                                    $precio_oferta = (int)get_post_meta(get_the_ID(), '_sale_price', true);
                                    if ($precio_oferta) {
                                        $valor_bono = ($precio_normal - $precio_oferta);
                                    } else {
                                        $valor_bono = (int)0;
                                    }
                                    $precio_final = ($precio_normal - $valor_bono);
                                }

                                if ($product->product_type == 'variable') {
                                    $available_variations = $product->get_available_variations();
                                    $variation_id = $available_variations[0]['variation_id'];
                                    $variable_product1 = new WC_Product_Variation($variation_id);

                                    $precio_normal = $variable_product1->regular_price;
                                    $precio_oferta = $variable_product1->sale_price;
                                    if ($precio_oferta) {
                                        $valor_bono = ($precio_normal - $precio_oferta);
                                    } else {
                                        $valor_bono = (int)0;
                                    }
                                    $precio_final = ($precio_normal - $valor_bono);
                                }
        
                                $autofin_modelo = $product->get_attribute('autofin');
                                $autofin_ano = $product->get_attribute('ano');
                                $modelo_moto = $product->get_attribute('modelo');
                                $moto_thumbs = get_the_post_thumbnail_url($product->ID, "medium");
                                if (empty($autofin_modelo)) {
                                    $use_autofin = 'no';
                                } else {
                                    $use_autofin = 'si';
                                }

                                $terms = get_the_terms($product->ID, 'product_cat');
                                foreach ($terms as $term) {
                                    $product_cat_name = $term->name;
                                    break;
                                }
?>
                            <li class="product col-sm-6 col-md-4 col-lg-3 pt-2 pb-4 text-center" data-price="<?php echo $precio_final; ?>">
                                <a href="<?php echo get_the_permalink(); ?>" class="woocommerce-LoopProduct-link">
                                    <img src="<?php echo esc_url($moto_thumbs); ?>" class="size-woocommerce_thumbnail w-100" alt="<?php echo get_the_title(); ?>">
                                    <h2 class="woocommerce-loop-product__title bk-woocommerce--shop__title text-center"><?php echo get_the_title(); ?></h2>
                                    <!-- <button class="conocela bk-woocommerce--shop__btn bk--btn bk--btn__primary d-block">Conócela &gt;</button> -->
                                </a>
                                <div class="cotizar_moto">
                                    <input type="hidden" name="model" value="<?php echo get_the_title(); ?>">
                                    <input type="hidden" name="use_autofin" value="<?php echo $use_autofin; ?>">
                                    <input type="hidden" name="autofin_model" value="<?php echo $autofin_modelo; ?>">
                                    <input type="hidden" name="model_number" value="<?php echo $autofin_ano; ?>">
                                    <input type="hidden" name="value_vehicle" value="<?php echo $precio_normal; ?>">
                                    <input type="hidden" name="brand_vehicle" value="kymco">
                                    <input type="hidden" name="value_bono" value="<?php echo $valor_bono; ?>">
                                    <input type="hidden" name="value_total" value="<?php echo $precio_final; ?>">
                                    <input type="hidden" name="moto_thumb" value="<?php echo esc_url($moto_thumbs); ?>">
                                    <input type="hidden" name="moto_url" value="<?php echo get_the_permalink(); ?>">
                                    <input type="hidden" name="moto_cat" value="<?php echo $product_cat_name; ?>">
                                    <button class="conocela bk-woocommerce--shop__btn bk--btn bk--btn__primary d-block btn_cotizar">Cotizar &gt;</button>
                                </div>
                            </li>
<?php
                            }
                        endwhile;
                        wp_reset_query();
                endif;
?>
                        </ul>
                    </div>
                </div>

                <!-- contenido 2 -->
                <div class="tab-pane fade" id="pills-simulacion" role="tabpanel" aria-labelledby="pills-simulacion-tab">
                    <div class="form-row">
                        <div class="col-5">
                            <h3 id="modelo_cotizar" class="text-center"></h3>
                            <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-cot-moto-1.png" alt="" class="thumb_cotizar" style="width:100%;max-width:100%;">
                        </div>
                        
                        <div class="col-7">
                            <div style="display:none;" id="caja_formulario_original">
                                <?php echo do_shortcode('[contact-form-7 id="115" title="Cotización" html_id="formulario_original"]') ?>
                            </div>
                            <div role="form" class="wpcf7" id="wpcf7-f115-o1" lang="es-ES" dir="ltr">
                                <form action="" method="post" class="wpcf7-form" novalidate="novalidate" name ="formulario_cotizar" id="formulario_cotizar">

                                    <div class="form-group col-12 caja_opcion_financiar" style="display:none;git">
                                        <p><b>¿Necesita financiamiento?</b></p>
                                        <label class="bk-check--container">
                                            <input type="radio" name="financiamiento" value="evaluar" checked>SI
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="bk-check--container">
                                            <input type="radio" name="financiamiento" value="cotizar">NO
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="form-row">
                                        <div style="display:none">
                                        	<input type="hidden" name="_wpcf7" value="115">
						    		        <input type="hidden" name="_wpcf7_version" value="5.0.5">
					            			<input type="hidden" name="_wpcf7_locale" value="es_ES">
							            	<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f115-o1">
								            <input type="hidden" name="_wpcf7_container_post" value="0"> 

                                            <input type="hidden" name="moto_ano" value="" >
                                            <input type="hidden" name="moto_modelo" value="" >
                                            <input type="hidden" name="moto_autofin" value="" >
                                            <input type="hidden" name="moto_sku" value="" >
                                            <input type="hidden" name="moto_marca" value="kymco" >
                                            <input type="hidden" name="moto_cat" value="">
                                            <input type="hidden" name="moto_color" value="generic" >
                                            <input type="hidden" name="moto_thumb" value="" >
                                            <input type="hidden" name="moto_precio" value="" >
                                            <input type="hidden" name="moto_bono" value="" >
                                            <input type="hidden" name="moto_precio_final" value="" >
                                            <input type="hidden" name="moto_precio_p" value="" >
                                            <input type="hidden" name="moto_bono_p" value="" >
                                            <input type="hidden" name="moto_precio_final_p" value="" >
                                            <input type="hidden" name="moto_url" value="" >
                                            <input type="hidden" name="moto_nombre" value="" >
                                            <input type="hidden" name="ces_autofin" value="no" >
                                            <input type="hidden" name="ces_email" value="" >
                                            <input type="hidden" name="ces_nombre" value="" >
                                            <input type="hidden" name="zon_nombre" value="" >
                                            <input type="hidden" name="cot_origen" value="Financiamiento">
                                            <input type="hidden" name="url_cotizacion" value="">

                                            <input type="hidden" name="cot_fecha" value="">
                                            <input type="hidden" name="utm_source" value="">
                                            <input type="hidden" name="utm_medium" value="">
                                            <input type="hidden" name="utm_campaign" value="">
                                            <input type="hidden" name="utm_content" value="">
                                            <input type="hidden" name="utm_term" value="">
                                            <input type="hidden" name="utm_code" value="">
                                        </div>
                                        <div class="col-12">
                                            <p><b>Datos personales</b></p>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="text" name="contacto_nombre" id="contacto_nombre" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control required" aria-required="true" placeholder="Nombre">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="text" name="contacto_apellido" id="contacto_apellido" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control required" aria-required="true" placeholder="Apellido">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="text" name="contacto_rut" id="contacto_rut" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control Rut required" aria-required="true" placeholder="Rut">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input type="tel" name="contacto_telefono" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel form-control required" aria-required="true" placeholder="Teléfono">
                                        </div>
                                        <div class="form-group col-12">
                                            <input type="email" name="contacto_email" id="contacto_email" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control required" aria-required="true" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                    <div class="col-12">
									<p><b>Información de Concesionario</b></p>
								</div>
								<div class="form-group col-md-6">
									<select name="ces_zona" id="bk_zona" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control required" aria-required="true">
										<option value="" selected>Seleccione su Región</option>
                                    <?php
                                    $taxonomy = 'ubicaciones';
                                    $terms_args = array(
                                        'orderby' => 'title',
                                        'hide_empty' => true,
                                        'parent' => 0
                                    );
                                    $terms = get_terms($taxonomy, $terms_args); // Get all top level terms of a taxonomy
                                    if ($terms && !is_wp_error($terms)) :
                                        foreach ($terms as $term) {
                                        ?>
                                            <option value="<?php echo $term->name; ?>"><?php echo $term->name; ?></option>
                                        <?php 
                                        }
                                        endif;
                                        ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <select name="ces_comuna" id="bk_comuna" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control required" aria-required="true">
                                            <option value="">Selecione una regíon primero...</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <select name="ces_concesionario" id="bk_concesionario" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required form-control required" aria-required="true">
                                            <option value="">Selecione una comuna primero...</option>
                                        </select>
                                    </div>
                                        <div class="form-group col-12 caja_nota_concesionario" style="display:none;">
                                            El Concesionario seleccionado no cuenta con opciones de financiamiento.
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn evaluame bk--btn bk--btn__primary" id="evaluame"> Evaluar > </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- contenido 3 -->
                <div class="tab-pane fade" id="pills-autofin" role="tabpanel" aria-labelledby="pills-autofin-tab">
                    <iframe src="" id="cajaAutofin" class="caja_financiamiento_autofin" frameborder="0" style="width:100%;min-width:992px;height:auto;min-height:740px;overflow:hidden; background:url('<?php bloginfo('template_directory'); ?>/assets/img/loading.gif') white center center no-repeat; background-size:50%; border:0px;"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('includes/footer'); ?>


