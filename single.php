<?php get_template_part('includes/header'); ?>
<section class="bk-single--img text-center">
<?php 
if (is_single('promociones')){
  the_post_thumbnail('full', ['class'=>'w-100']); 
}
?>
</section>
<section class="mt-5">
<?php get_template_part('includes/loops/single-post', get_post_format()); ?>
</section>

<?php
if (is_singular('promociones')) {

  get_template_part('includes/loops/slides/home-product');
}
?>

<?php get_template_part('includes/footer'); ?>
<?php if (is_singular('concesionarios')): 
  get_template_part('includes/map'); 
endif; ?>