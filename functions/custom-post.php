<?php

// Custom post type concesionarios
if ( ! function_exists('derco_concesionarios') ) {

	// Register Custom Post Type
	function derco_concesionarios() {
	
		$labels = array(
			'name'                  => _x( 'Concesionarios', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Concesionario', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Concesionarios', 'text_domain' ),
			'name_admin_bar'        => __( 'Concesionarios', 'text_domain' ),
			'archives'              => __( 'Item Archives', 'text_domain' ),
			'attributes'            => __( 'Item Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'Todos los concesionarios', 'text_domain' ),
			'add_new_item'          => __( 'Agregar nuevo concesionario', 'text_domain' ),
			'add_new'               => __( 'Agregar nuevo', 'text_domain' ),
			'new_item'              => __( 'Nuevo concesionario', 'text_domain' ),
			'edit_item'             => __( 'Editar concesionario', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'Ver concesionario', 'text_domain' ),
			'view_items'            => __( 'Ver concesionarios', 'text_domain' ),
			'search_items'          => __( 'Buscar concesionario', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( '', 'text_domain' ),
			'set_featured_image'    => __( '', 'text_domain' ),
			'remove_featured_image' => __( '', 'text_domain' ),
			'use_featured_image'    => __( '', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Items list', 'text_domain' ),
			'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Concesionario', 'text_domain' ),
			'description'           => __( 'Concesionarios Derco Motos', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'revisions' ),
			'taxonomies'            => array( 'ubicaciones' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'         	=> 'dashicons-location',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
			'rewrite' => array( 'slug' => 'concesionarios', 'with_front'=> false ),
		);
		register_post_type( 'concesionarios', $args );

		register_taxonomy( 'ubicaciones', array('concesionarios'), array(
			'hierarchical' => true, 
			'label' => 'Tipos', 
			'singular_label' => 'Tipo', 
			'rewrite' => array( 'slug' => 'ubicaciones', 'with_front'=> false )
			)
		);
	
		register_taxonomy_for_object_type( 'ubicaciones', 'concesionarios' );
	
	}
	add_action( 'init', 'derco_concesionarios', 0 );
	
}
// Custom post type promociones
if ( ! function_exists('derco_promociones') ) {

	// Register Custom Post Type
	function derco_promociones() {
	
		$labels = array(
			'name'                  => _x( 'Promociones', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Promocion', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Promociones', 'text_domain' ),
			'name_admin_bar'        => __( 'Promociones', 'text_domain' ),
			'archives'              => __( 'Item Archives', 'text_domain' ),
			'attributes'            => __( 'Item Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'Todos los promociones', 'text_domain' ),
			'add_new_item'          => __( 'Agregar nuevo promocion', 'text_domain' ),
			'add_new'               => __( 'Agregar nuevo', 'text_domain' ),
			'new_item'              => __( 'Nuevo promocion', 'text_domain' ),
			'edit_item'             => __( 'Editar promocion', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'Ver promocion', 'text_domain' ),
			'view_items'            => __( 'Ver promociones', 'text_domain' ),
			'search_items'          => __( 'Buscar promocion', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
			'set_featured_image'    => __( 'Agregar Imagen destacada', 'text_domain' ),
			'remove_featured_image' => __( 'Remover Imagen destacada', 'text_domain' ),
			'use_featured_image'    => __( 'Usar Imagen destacada', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Items list', 'text_domain' ),
			'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Promocion', 'text_domain' ),
			'description'           => __( 'Promociones Derco Motos', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail' ),
			'taxonomies'            => array( 'category', 'post_tag' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'         	=> 'dashicons-megaphone',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);
		register_post_type( 'promociones', $args );
	
	}
	add_action( 'init', 'derco_promociones', 0 );
	
}
// Custom post type eventos
if ( ! function_exists('derco_eventos') ) {

	// Register Custom Post Type
	function derco_eventos() {
	
		$labels = array(
			'name'                  => _x( 'Eventos', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Evento', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'kymco Eventos', 'text_domain' ),
			'name_admin_bar'        => __( 'Royal Eventos', 'text_domain' ),
			'archives'              => __( 'Item Archives', 'text_domain' ),
			'attributes'            => __( 'Item Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'Todos los eventos', 'text_domain' ),
			'add_new_item'          => __( 'Agregar nuevo evento', 'text_domain' ),
			'add_new'               => __( 'Agregar nuevo', 'text_domain' ),
			'new_item'              => __( 'Nuevo evento', 'text_domain' ),
			'edit_item'             => __( 'Editar evento', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'Ver evento', 'text_domain' ),
			'view_items'            => __( 'Ver eventos', 'text_domain' ),
			'search_items'          => __( 'Buscar evento', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
			'set_featured_image'    => __( 'Agregar Imagen destacada', 'text_domain' ),
			'remove_featured_image' => __( 'Remover Imagen destacada', 'text_domain' ),
			'use_featured_image'    => __( 'Usar Imagen destacada', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Items list', 'text_domain' ),
			'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Evento', 'text_domain' ),
			'description'           => __( 'Eventos Derco Motos', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail' ),
			'taxonomies'            => array( 'tipo' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'         	=> get_bloginfo('template_directory') . '/assets/img/bk-experiencer-icon.png',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);
		register_post_type( 'experience', $args );

		register_taxonomy( 'eventype', array('experience'), array(
			'hierarchical' => true, 
			'label' => 'Categorías de eventos', 
			'singular_label' => 'Categoría de evento', 
			'rewrite' => array( 
				'slug' => 'eventype', 
				'with_front'=> false 
				)
			)
		);
	
		register_taxonomy_for_object_type( 'tipo', 'experience' );
	
	}
	add_action( 'init', 'derco_eventos', 0 );
	
}

// Home slider diapos
function custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Diapositivas', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Diapositiva', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Home Slider', 'text_domain' ),
		'name_admin_bar'        => __( 'Home Slider', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Diapositiva', 'text_domain' ),
		'description'           => __( 'Diapositivas del home', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'home_slider', $args );

}
add_action( 'init', 'custom_post_type', 0 );
?>