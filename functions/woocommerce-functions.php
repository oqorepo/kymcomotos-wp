<?php
/* =====================================================
Pagina de producto 
===================================================== */

// Declaracion de compatibilidad con woocommerce y lightbox de galería por defecto
add_action('after_setup_theme', 'woocommerce_support');
function woocommerce_support()
{
    add_theme_support('woocommerce', array(
        'thumbnail_image_width' => 250,
        'gallery_thumbnail_image_width' => 100,
        'single_image_width' => 500,
    ));

    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
}
// Remover editor
function remove_product_editor()
{
    remove_post_type_support('product', 'editor');
}
add_action('init', 'remove_product_editor');

// Hooks Pagina de Producto Woocommerce
if ( !function_exists('derco_motos_pagina_producto_cambios_hooks') ) :
    function derco_motos_pagina_producto_cambios_hooks(){
        // caja de precios e imagen en el header
        add_action('woocommerce_before_single_product', 'woocommerce_pagina_producto_header', 2);
        function woocommerce_pagina_producto_header(){
            // recogemos las variables de precio del producto
            global $product;
            
            if ($product->is_type('simple')) {
                $precio_normal = (int)get_post_meta(get_the_ID(), '_regular_price', true);;
                $precio_oferta = (int)get_post_meta(get_the_ID(), '_sale_price', true);
                $currentID = get_the_ID();
                $valor_bonoautofin = get_field('valor_bono', $currentID);
                if ($precio_oferta) {
                    $valor_bono = ($precio_normal - $precio_oferta);
                } else {
                    $valor_bono = (int)0;
                }
                $precio_final = ($precio_normal - $valor_bono);
                $precio_con_autofin = intval($precio_final) - intval($valor_bonoautofin);
            }

            if ($product->product_type == 'variable') {
                #Step 1: Get product varations
                $available_variations = $product->get_available_variations();
                #Step 2: Get product variation id
                $variation_id = $available_variations[0]['variation_id']; // Getting the variable id of just the 1st product. You can loop $available_variations to get info about each variation.
                #Step 3: Create the variable product object
                $variable_product1 = new WC_Product_Variation($variation_id);
                #Step 4: You have the data. Have fun :)
                $precio_normal = $variable_product1->regular_price;
                $precio_oferta = $variable_product1->sale_price;
                if ($precio_oferta) {
                    $valor_bono = ($precio_normal - $precio_oferta);
                } else {
                    $valor_bono = (int)0;
                }
                $precio_final = ($precio_normal - $valor_bono);

                foreach ( $product->get_available_variations() as $key ) {
                    $attr_string[] = "'".$key["variation_id"]."'";
                }
                $id_var = implode($attr_string,',');
            }

            // verificamos que se pueden leer los custom fields
            if (function_exists('the_field')) {
                echo '<section class="container-fluid bk-product--banner">';
                echo ' <div class="container">';
                echo '  <div class="row align-items-center">';
                echo '<input type="hidden" name="product_id" value="';
                if ($product->product_type == 'variable') {
                    echo '['.$id_var.']' ;
                }else{
                    echo "'".$product->get_id()."'";
                }
                echo '">';
                echo '   <div id="moto_autofin" class="col-sm-6">
                    <div class="bk-shop--banner__txt">';
                    // precio final (normal o con bono)
                if (get_field('valor_bono')) {echo '    <div class="normal bk-shop--banner__price-normal">
                                <h4 style="color: #ed1c24;">Desde:</h4>
                                <h2>$ ' . number_format($precio_con_autofin, 0, ",", ".") . '</h2>
                            </div>';
                } else {
                    echo '    <div class="normal bk-shop--banner__price-normal">
                                <h4 style="color: #ed1c24;">Desde:</h4>
                                <h2>$ ' . number_format($precio_final, 0, ",", ".") . '</h2>
                            </div>';
                }
                    // si el producto esta en oferta mostramos bono
                if (!empty($precio_oferta)) {
                    echo '<div class="bono bk-shop--banner__price-bono">
                                <h4>Incluye bono: $ ' . number_format($valor_bono, 0, ",", ".") . '</h4>
                            </div>';
                }
                if (get_field('valor_bono')) {
                    echo '<div class="bono-autofin bk-shop--banner__price-bono">
                    <h4>Bono de Financiamiento: $ ' . number_format($valor_bonoautofin, 0, ",", ".") . '</h4>
                    </div>';
                }
                    // si el producto esta en oferta mostramos precio normal
                if (!empty($precio_oferta)) {
                    echo '<div class="bono bk-shop--banner__price-bono">
                                <small>Precio Normal: <span class="bk-shop--banner__price-bono-n">$ ' . number_format($precio_normal, 0, ",", ".") . '</span></small>
                            </div>';
                }

                // si producto tiene autofin mostramos valor cuota y link a financiamiento
                $autofin_modelo = $product->get_attribute('autofin');
                $link_ecommerce = get_post_meta( $product->get_id(), 'link_ecommerce', true );
                if (!empty($autofin_modelo)) {
                    $autofin_ano = $product->get_attribute('ano');
                    $modelo_moto = $product->get_attribute('m-modelo');
                    echo '<div class="autofin-container hidden"> 
                                <small class="cae" ></small>
                                <span class="price"></span>
                                <input type="hidden" name="model" value="' . $modelo_moto . '">
                                <input type="hidden" name="use_autofin" value="">
                                <input type="hidden" name="autofin_model" value="' . $autofin_modelo . '">
                                <input type="hidden" name="model_number" value="' . $autofin_ano . '">
                                <input type="hidden" name="value_vehicle" value="' . $precio_normal . '">
                                <input type="hidden" name="brand_vehicle" value="KYMCO">
                                <input type="hidden" name="value_bono" value="' . $valor_bono . '">
                                <input type="hidden" name="value_total" value="' . $precio_final . '">
                            </div>';
                    echo '<div class="mt-2 boton"><button class="cd-nav-trigger bk-woocommerce--shop__btn bk--btn bk--btn__primary bk-cotizar--producto">Cotizar ></button></div>';
                    if (!empty($link_ecommerce)) {echo '<a href="' . $link_ecommerce . '"><div class="mt-2 boton"><button class="bk-woocommerce--shop__btn bk--btn bk--btn__primary">Comprar ></button></div></a>';};
                } else {
                    echo '<div class="mt-2 boton"><button class="cd-nav-trigger bk-woocommerce--shop__btn bk--btn bk--btn__primary bk-cotizar--producto">Cotizar ></button></div>';
                    if (!empty($link_ecommerce)) {echo '<a href="' . $link_ecommerce . '"><div class="mt-2 boton"><button class="bk-woocommerce--shop__btn bk--btn bk--btn__primary">Comprar ></button></div></a>';};
                }
                    // termina la etapa de autofin 
                echo '   </div>';
                echo '   </div><div class="col-sm-6 bk-shop--banner__img">';
                $img_product_wc = get_field('imagen_header');
                $img_product_wc_size = 'medium';
                if ($img_product_wc) {
                    echo '<img class="w-100" src=' . $img_product_wc . '>';
                } else {
                    if (have_rows('grupo_multimedia')) {
                        while (have_rows('grupo_multimedia')) : the_row();
                        $image = get_sub_field('multimedia_moto');
                        echo '<img class="w-100" src=' . $image['sizes']['medium'] . '>';
                        endwhile;
                    }
                }
                echo '   </div>';
                echo '  </div>';
                echo ' </div>';
                echo '</section>';
            }
        }

        // subimos el bloque de precio al bloque superior
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        //add_action('woocommerce_before_single_product', 'woocommerce_template_single_price', 2);

        /*add_filter('woocommerce_empty_price_html', 'custom_call_for_price');

        function custom_call_for_price() {
            return 'Call for price';
        }
     */

        //quitamos productos relacionados, elementos meta y sku
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20); //productos relacionados
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40); // categoria
    add_filter('wc_product_sku_enabled', '__return_false'); //sku de producto

        //inicio section detalle de moto
    add_action('woocommerce_before_single_product_summary', 'woocommerce_pagina_producto_seccion_moto_inicio', 1);
    function woocommerce_pagina_producto_seccion_moto_inicio()
    {
        echo '
                <section class="container">
                    <div class="row text-center">
            ';
    }
        // columna de caracteristicas
    add_action('woocommerce_before_single_product_summary', 'woocommerce_pagina_producto_caracteristicas', 2);
    function woocommerce_pagina_producto_caracteristicas() {

    }
        // abre columna de imagenes
    add_action('woocommerce_before_single_product_summary', 'woocommerce_pagina_producto_caja_imagen_inicio', 3);
    function woocommerce_pagina_producto_caja_imagen_inicio()
    {
        echo '<div class="col-md-6"><style>.woocommerce-page div.product div.images { width: 100%; }.single_variation_wrap{display:none !important;}</style>';
    }

        // cierra columna de imagenes y abre columna de titulo y descripcion
    add_action('woocommerce_single_product_summary', 'woocommerce_pagina_producto_caja_descripcion_inicio', 1);
    function woocommerce_pagina_producto_caja_descripcion_inicio()
    {
        echo '</div>';
        echo '<div class="col-md-6 align-self-center">';
    }

    add_filter('woocommerce_is_sold_individually', 'woocommerce_pagina_producto_elimina_precio', 10, 2);
    function woocommerce_pagina_producto_elimina_precio($return, $product)
    {
        return true;
    }

        // Elimina opciones de variaciones
        //remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30); //removes the add to cart button on the single product page    

        // CTA Cotizar
    add_filter('woocommerce_available_variation', function ($value, $object = null, $variation = null) {
        if ($value['price_html'] == '') {
            $value['price_html'] = '<span class="price">' . $variation->get_price_html() . '</span>';
        }
        return $value;
    }, 10, 3);
        //add_action('woocommerce_after_add_to_cart_form', 'woo_woocommerce_call_to_action_button2', 1);
    function woo_woocommerce_call_to_action_button2()
    {
        global $product; //get the product object
        if ($product->is_type('variable')) {
            foreach ($product->get_available_variations() as $variation) {
                // Variation ID
                $variation_id = $variation['variation_id'];
                echo '<div class="product-variation variation-id-' . $variation_id . '">
                    <strong>Variation id</strong>: ' . $variation_id . '<br>';

                // Attributes
                $attributes = array();
                foreach ($variation['attributes'] as $key => $value) {
                    $taxonomy = str_replace('attribute_', '', $key);
                    $taxonomy_label = get_taxonomy($taxonomy)->labels->singular_name;
                    $term_name = get_term_by('slug', $value, $taxonomy)->name;
                    $attributes[] = $taxonomy_label . ': ' . $term_name;
                }
                echo '<span class="variation-attributes">
                    <strong>Attributes</strong>: ' . implode(' | ', $attributes) . '</span><br>';

                // Prices
                $active_price = floatval($variation['display_price']); // Active price
                $regular_price = floatval($variation['display_regular_price']); // Regular Price
                if ($active_price != $regular_price) {
                    $sale_price = $active_price; // Sale Price
                }
                echo '<span class="variation-prices">
                    <strong>Price</strong>: ' . $variation['price_html'] . '</span><br>
                </div>';
            }
        }
            /*
            if ($product) { // if there's a product proceed
                $url = esc_url($product->get_permalink()); //product url
                echo '<div class="boton"><button class="cd-nav-trigger bk-woocommerce--shop__btn bk--btn bk--btn__primary">Cotizar ' . $product->name . ' ></button></div>';
            }
         */
    }
        

    // cierra columna de titulo y descripcion
    add_action('woocommerce_after_add_to_cart_form', 'woocommerce_pagina_producto_caja_descripcion_fin', 2);
    function woocommerce_pagina_producto_caja_descripcion_fin()
    {
        echo '</div>';
    }    

    //fin section detalle de moto
    add_action('woocommerce_after_single_product_summary', 'woocommerce_pagina_producto_seccion_moto_fin', 11);
    function woocommerce_pagina_producto_seccion_moto_fin()
    {
        //Apertura del contenedor de woocomers IMPORTANTE
        echo '
                </div>
            </section>
        ';
    }

    // parametros fijos de producto bajo la imagen y descripcion
    add_action('woocommerce_after_single_product_summary', 'woocommerce_template_custom_content', 12);
    function woocommerce_template_custom_content()
    {
        global $product;
        $alturaAsiento = $product->get_attribute('altura-del-asiento');
        //$aroRueda = $product->get_attribute('aro-de-rueda');
        //$cambios = $product->get_attribute('cambios');
        $cilindrada = $product->get_attribute('cilindrada');
        //$controlTraccion = $product->get_attribute('control-de-traccion');
        $frenoDelantero = $product->get_attribute('freno-delantero');
        $frenoTrasero = $product->get_attribute('freno-trasero');
        $hpTorque = $product->get_attribute('par');
        //$recorridoSuspencion = $product->get_attribute('recorrido-de-suspension');
        //$tipoRueda = $product->get_attribute('tipo-de-rueda');
        $traccion = $product->get_attribute('traccion');
        //$tipoRueda = $product->get_attribute('tipo-de-rueda');
        $potencia = $product->get_attribute('potencia');
        $transmision = $product->get_attribute('transmision');
        $estanque = $product->get_attribute('tanque-de-combustible');
        $peso = $product->get_weight();
    
        $terms = get_the_terms($product->ID, 'product_cat');

        foreach ($terms as $term) $categories[] = $term->slug;
        //Cierre del contenedor de woocomers IMPORTANTE </section>
        echo '
            <section>
        ';

        if (have_rows('grupo_multimedia')) {


            //echo '<table class="table" style="border:none !important;">';

            while (have_rows('grupo_multimedia')) : the_row();
            $image = get_sub_field('multimedia_moto');
            $vista = get_sub_field('360_url');
            $vista360 = site_url('360/') . $vista;
            $sonido = get_sub_field('sonido');

            echo '
            <div style="position:relative;z-index:2;">
                <div id="hotspotImg" class="responsive-hotspot-wrap">
                    <img class="bk-tool-img" src="' . $image['sizes']['large'] . '" alt="' . $image['alt'] . '" />
            ';
            if (have_rows('tooltips')) {
                while (have_rows('tooltips')) : the_row();
                $tooltipY = get_sub_field('posicion_vertical');
                $tooltipX = get_sub_field('posicion_hotizontal');
                $tooltipTxt = get_sub_field('contenido');

                echo '<div class="hot-spot" x="' . $tooltipX . '" y="' . $tooltipY . '">
                        <div class="circle"></div>
                        <div class="tooltip">
                            <b>' . $tooltipTxt . '</b>
                        </div>
                    </div>
                ';
                endwhile;
            }
            echo '</div>';

            
            
            echo '</div>';
            endwhile;
        }
        echo '
            <div class="bk-triangle-grey">
            </div>
            </section>';

            if (have_rows('highlight')) {
                echo '<section class="bk-high--container-fluid" ><ul class="owl-carousel owl-theme bk-high--container">';

                    $field = get_field_object('highlight');
                    $highlights = get_field('highlight');

                    //var_dump($highlights);
                    foreach( $highlights as $key => $val ){
                             
                        $label = $highlights[$key];
                    
                
                        echo '<li class="pt-1 pb-1">
                                <span class="mr-2 bk-high--icon__img '.$val.'"></span>
                                <br>
                                <p class="bk-high--icon__txt"><b>'.$field['choices'][$label].'</b></p>
                            </li>';
                        
                    };
                    echo '</ul></section>';

                    if (wp_is_mobile()) {
                        echo '<div class="bk-bg-grey"><img class="shake-horizontal" src="'.get_template_directory_uri().'/assets/img/swipe.png" alt="icono-swipe" style="width:40px;height:40px;"></div>';
                    } else {
                        # nada...
                    };
                
                
            }
                echo '
            <section class="container-fluid p-5 cb bk-bg-grey">
                <div class="bk-block-container">
                    <div class="row text-center">
                        <div class="col-12 bk--title">
        ';
        
            echo '
                        <h2>Todos los caminos son tuyos</h2>
                        </div>
                    </div>
                    <ul class="row text-center justify-content-center bk-product--attr__ul">
                    
                        <li class="bk-product--attr__li"><p class="bk-product--attr__title">Cilindrada</p><h4>' . $cilindrada . '</h4></li>
                        <li class="bk-product--attr__li"><p class="bk-product--attr__title">Capacidad de Estanque</p><h4>' . $estanque . '</h4></li>
                        <li class="bk-product--attr__li"><p class="bk-product--attr__title">Peso</p><h4>' . $peso . ' kgs</h4></li>
                        <li class="bk-product--attr__li"><p class="bk-product--attr__title">Altura del asiento</p><h4>' . $alturaAsiento . '</h4></li>
            ';
        
        echo '      </div>
                </div>
            </section>
        ';
        
    }
    //se agrega boton del comparador
    add_action('woocommerce_after_single_product_summary', 'woocommerce_template_btn_comparador', 16);
    function woocommerce_template_btn_comparador()
    {
        echo '
            <section class="container-fluid cb bk-bg-grey pb-5 bk-wc--buttons">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-12">
            ';
        echo do_shortcode('[yith_compare_button]');
    }
    //boton descarga de ficha
    add_action('woocommerce_after_single_product_summary', 'woocommerce_template_btn_especificaciones', 17);
    function woocommerce_template_btn_especificaciones()
    {
        if (get_field('especificaciones')) {
            echo '<a href="' . get_field('especificaciones') . '" class="button" target="_blank">Ficha Técnica</a>';
        } else {
            echo '<a href="#" class="button" target="_blank">Ficha Técnica</a>';
        }
        echo '
                        </div>
                    </div>
                </div>
            </section>
            ';
    }

    //galeria de imagenes 
    add_action('woocommerce_after_single_product_summary', 'woocommerce_template_galeria', 18);
    function woocommerce_template_galeria()
    {
        if (wp_is_mobile()) {
            # nada...
        } else {
            if (function_exists('the_field')) {
                $videoUrl = get_field('enlace_video');
                if ($videoUrl) {
                    echo "<div id='video'></div>";
                }
            }
        }
/*         if (function_exists('the_field')) {

            $carousel_item = get_field('galeria_imagenes');
            $size = 'large';
            $count = 0;
            $count1 = 0;
            if ($carousel_item) {
                echo '<section class="bk-product-gallery--banner">';
                echo '
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                ';
                foreach ($carousel_item as $image) {
                    echo '<li data-target="#carouselExampleIndicators" data-slide-to="' . $count . '" ';
                    if ($count == 0) {
                        echo ' class="active"';
                    }
                    echo '></li>';
                    $count++;
                }
                echo '</ol>';

                echo '<div class="carousel-inner">';
                foreach ($carousel_item as $image) {
                    echo '<div class="carousel-item';
                    if ($count1 == 0) {
                        echo ' active';
                    }
                    echo '">
                                <img class="d-block w-100" src="' . $image['sizes'][ $size ] . '" alt="' . $image['alt'] . '" />
                            </div>';
                    $count1++;
                }
                echo '</div></section>';
            }
        } */
    };

    /* Remueve tabs de pagina de producto */
    function woo_remove_product_tab($tabs)
    {
        unset($tabs['description']);                 // Quitar la pestaña de descripción larga
        unset($tabs['reviews']);                     // Quitar la pestaña de reseñas
        unset($tabs['additional_information']);      // Quitar la pestaña de información adicional
        return $tabs;
    };
    add_filter('woocommerce_product_tabs', 'woo_remove_product_tab', 98);

    add_filter('woocommerce_get_image_size_gallery_thumbnail', function ($size) {
        return array(
            'width' => 150,
            'height' => 100,
            'crop' => 0,
        );
    });
};
endif;

add_action('after_setup_theme', 'derco_motos_pagina_producto_cambios_hooks');





/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter('loop_shop_per_page', 'new_loop_shop_per_page', 20);

function new_loop_shop_per_page($cols)
{
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
    $cols = 40;
    return $cols;
}



/* pagina de tienda - categoría */
// Hooks Pagina de Tienda Woocommerce
if (!function_exists('derco_motos_pagina_tienda_cambios_hooks')) :
    function derco_motos_pagina_tienda_cambios_hooks()
{

    // Quita ordenador de resultados de paginas woocommerce
    remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

    // Quita contador de resultados de paginas woocommerce
    remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

    //Oculta titulo de la pagina de catálogo
    add_filter('woocommerce_show_page_title', 'dercomotos_pagina_tienda_oculta_titulo');
    function dercomotos_pagina_tienda_oculta_titulo()
    {
        if (is_shop()) {
            return false;
        }
    }
    

    add_action('woocommerce_before_shop_loop', 'dercomotos_pagina_tienda_filtro', 4);
    function dercomotos_pagina_tienda_filtro() { 
        //Banner Superior
        echo '<section class="container-fluid bk-shop--banner"></section>';
        //Sección contenedora de widget
        echo '<section class=" container"><div class="row pt-5">';
        if(is_active_sidebar('shop-widget-area')){
            dynamic_sidebar('shop-widget-area');
        }
        echo '</div></section>';
    }

    //inicio funcion para lista de categorias de woocmmerce solo en pagina de tienda para selector
    add_action('woocommerce_before_shop_loop', 'dercomotos_pagina_tienda_lista_categorias', 6);
    function dercomotos_pagina_tienda_lista_categorias()
    {
        if (is_shop()) {
            $taxonomy = 'product_cat';
            $orderby = 'name';
            $show_count = 0;      // 1 for yes, 0 for no
            $pad_counts = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;    // 1 for yes, 0 for no  
            $title = '';
            $empty = 0;

            $args = array(
                'taxonomy' => $taxonomy,
                'orderby' => $orderby,
                'show_count' => $show_count,
                'pad_counts' => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li' => $title,
                'hide_empty' => $empty,
                'exclude' => 17,
            );
            $all_categories = get_categories($args);

            if(wp_is_mobile()){
                echo '<div class="container">
                <select id="js_selector_cat_mb" name="select_type_cat" class="custom-select custom-select-lg mb-3">
                <option value="todas" selected>Seleccionar tipo</option>';
                foreach ($all_categories as $cat) {
                    if ($cat->category_parent == 0) {
                        $category_id = $cat->term_id;
                        echo '<option value="' . $cat->slug . '">' . $cat->name . '</option>';    
                    }
                }
                echo '</select></div>';
            }else{

                //inicio de caja de selector
                echo '<div class="container"><div class="row pt-5 pb-5"><ul class="bk-wc-category--list">';
                echo '<li><a id="todas" class="bk-wc-category--list__item active">TODAS</a></li>';
                foreach ($all_categories as $cat) {
                    if ($cat->category_parent == 0) {
                        $category_id = $cat->term_id;
                        echo '<li><a id="' . $cat->slug . '" class="bk-wc-category--list__item">' . $cat->name . '</a></li>';
                        //echo '<div class="col"><a href="' . get_term_link($cat->slug, 'product_cat') . '">' . $cat->name . '</a></div>';
    
                        // para cuando hay subcategorias
    
                        $args2 = array(
                            'taxonomy' => $taxonomy,
                            'child_of' => 0,
                            'parent' => $category_id,
                            'orderby' => $orderby,
                            'show_count' => $show_count,
                            'pad_counts' => $pad_counts,
                            'hierarchical' => $hierarchical,
                            'title_li' => $title,
                            'hide_empty' => $empty
                        );
                        $sub_cats = get_categories($args2);
                        if ($sub_cats) {
                            foreach ($sub_cats as $sub_category) {
                                echo '<br/><a href="' . get_term_link($sub_category->slug, 'product_cat') . '">' . $sub_category->name . '</a>';
                                echo apply_filters('woocommerce_subcategory_count_html', ' <span class="cat-count">' . $sub_category->count . '</span>', $category);
                            }
                        }
    
                    }
                }
    
                //fin caja selector
                echo '</ul></div></div>';
            }

        }
    }
    //fin funcion para lista de categorias de woocmmerce solo en pagina de tienda para selector

    //abre y cierra caja de elementos en el loop de productos
    add_action('woocommerce_before_shop_loop', 'dercomotos_pagina_loop_abre_caja', 7);
    function dercomotos_pagina_loop_abre_caja()
    {
        echo '<div class="container">';
    }
    add_action('woocommerce_after_shop_loop', 'dercomotos_pagina_loop_cierra_caja', 15);
    function dercomotos_pagina_loop_cierra_caja()
    {
        echo '</div>';
    }

    // caja de producto 
    //remueve precio en el loop
    remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);


    remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
    add_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 12);

    /*
    function yith_woocompare_set_new_text_for_add_label($label) {
        $label = "Comparar";
        return $label;
    }
    add_filter('yith_woocompare_compare_text_label', 'yith_woocompare_set_new_text_for_add_label');
     */

    function yith_woocompare_set_new_text_for_added_label($label)
    {
        $label = "";
        return $label;
    }
    add_filter('yith_woocompare_compare_added_label', 'yith_woocompare_set_new_text_for_added_label');

    // Agrega titulo "conócela" extra bajo el nombre/modelo de producto
    add_action('woocommerce_before_shop_loop_item', 'dercomotos_pagina_tienda_boton_comparar', 0);
    function dercomotos_pagina_tienda_boton_comparar()
    {
        echo '<div style="position: absolute; right: 0;">' . do_shortcode('[yith_compare_button]') . '</div>';
    }

    // Agrega titulo "conócela" extra bajo el nombre/modelo de producto
    add_action('woocommerce_shop_loop_item_title', 'dercomotos_pagina_tienda_boton_conocelo', 11);
    function dercomotos_pagina_tienda_boton_conocelo()
    {
        echo '<button class="conocela">Conócela ></button>';
    }
    // Cierra Link de producto
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
    add_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 12);

    // Agrega titulo "conócela" extra bajo el nombre/modelo de producto
    add_action('woocommerce_shop_loop_item_title', 'dercomotos_pagina_tienda_boton_cotizar', 13);
    function dercomotos_pagina_tienda_boton_cotizar()
    {
        global $product;
        if ($product->is_type('simple')) {
            $precio_normal = (int)get_post_meta(get_the_ID(), '_regular_price', true);;
            $precio_oferta = (int)get_post_meta(get_the_ID(), '_sale_price', true);
            if ($precio_oferta) {
                $valor_bono = ($precio_normal - $precio_oferta);
            } else {
                $valor_bono = (int)0;
            }
            $precio_final = ($precio_normal - $valor_bono);
        }

        if ($product->product_type == 'variable') {
            $available_variations = $product->get_available_variations();
            $variation_id = $available_variations[0]['variation_id'];
            $variable_product1 = new WC_Product_Variation($variation_id);

            $precio_normal = $variable_product1->regular_price;
            $precio_oferta = $variable_product1->sale_price;
            if ($precio_oferta) {
                $valor_bono = ($precio_normal - $precio_oferta);
            } else {
                $valor_bono = (int)0;
            }
            $precio_final = ($precio_normal - $valor_bono);
        }
        $autofin_modelo = $product->get_attribute('autofin');
        $autofin_ano = $product->get_attribute('ano');
        $modelo_moto = $product->get_attribute('m-modelo');
        $moto_thumbs = get_the_post_thumbnail_url($product->ID, "full");

        if (empty($autofin_modelo)) {
            $use_autofin = 'no';
        } else {
            $use_autofin = 'si';
        }

        $terms = get_the_terms($product->ID, 'product_cat');

        foreach ($terms as $term) {
            $product_cat_name = $term->name;
            break;
        }

        echo '<div class="cotizar_moto">
                <input type="hidden" name="product_id" value="' . $product->get_id() . '">
                <input type="hidden" name="model" value="' . $modelo_moto . '">
                <input type="hidden" name="use_autofin" value="' . $use_autofin . '">
                <input type="hidden" name="autofin_model" value="' . $autofin_modelo . '">
                <input type="hidden" name="model_number" value="' . $autofin_ano . '">
                <input type="hidden" name="value_vehicle" value="' . $precio_normal . '">
                <input type="hidden" name="brand_vehicle" value="KYMCO">
                <input type="hidden" name="value_bono" value="' . $valor_bono . '">
                <input type="hidden" name="value_total" value="' . $precio_final . '">
                <input type="hidden" name="moto_thumb" value="' . esc_url($moto_thumbs) . '">
                <input type="hidden" name="moto_url" value="' . get_the_permalink() . '">
                <input type="hidden" name="moto_cat" value="' . $product_cat_name . '">
                <button class="cd-nav-trigger cotizar bk-woocommerce--shop__btn bk--btn bk--btn__primary bk--btn__dark">Cotizar ></button>
              </div>';
    }

    add_filter('woocommerce_loop_add_to_cart_link', 'woo_display_variation_dropdown_on_shop_page');

    function woo_display_variation_dropdown_on_shop_page()
    {
        echo 'variable';
    }

}
endif;
add_action('after_setup_theme', 'derco_motos_pagina_tienda_cambios_hooks');


/****************************/
/* FORMULARIO DE COTIZACION */
/****************************/

function action_wpcf7_mail_sent($contact_form)
{ 

    //verificamos estar en el servidor correcto
    if ($_SERVER['HTTP_HOST'] == 'kymcomotos.cl' || $_SERVER['HTTP_HOST'] == 'www.kymcomotos.cl') {


            $submission = WPCF7_Submission::get_instance();
            // print_r($submission);
         
        if ($submission) {
            $posted_data = $submission->get_posted_data();

            // Recogemos las varibales del form
            $firstname             = utf8_decode($posted_data['contacto_nombre']);
            $lastname              = utf8_decode($posted_data['contacto_apellido']);
            $rut                   = $posted_data['contacto_rut'];
            $email                 = $posted_data['contacto_email'];
            $phone                 = $posted_data['contactoTel'];
            $address               = $posted_data['address'];
            $comuna_personal       = $posted_data['comuna_personal'];
            $region_personal       = $posted_data['region_personal'];
            $model                 = $posted_data['moto_modelo'];
            $model_number          = $posted_data['moto_autofin'];
            $concessionaire_name   = utf8_decode($posted_data['ces_concesionario']);
            $concessionaire_comuna = utf8_decode($posted_data['ces_comuna']);
            $concessionaire_region = utf8_decode($posted_data['zon_nombre']);
            $value_vehicle         = $posted_data['moto_precio'];
            $value_bono            = ($posted_data['moto_bono'] != '') ? $posted_data['moto_bono'] : 0;
            $value_total           = $posted_data['moto_precio_final'];
            $url_cotizacion        = $posted_data['moto_url'];
            $pdf_url               = $posted_data['url-pdf'];
            $export                = '0';
            $origen                = $posted_data['cot_origen'];
            $financiamiento        = $posted_data['financiamiento'];
            $color                 = $posted_data['moto_color'];
            $categoria             = $posted_data['moto_cat'];
            $utm_source            = $posted_data['utm_source'];
            $utm_medium            = $posted_data['utm_medium'];
            $utm_campaign          = $posted_data['utm_campaign'];
            $utm_content           = $posted_data['utm_content'];
            $utm_term              = $posted_data['utm_term'];
            $utm_code              = $posted_data['utm_code'];
            $marca                 = $posted_data['moto_marca'];
            $id_ces                = $posted_data['ces_autofin'];
            $moto_ano              = $posted_data['moto_ano'];

            //Proceso de add en cotizaciones generales de dercomotos.cl

            //Servidor de Cotizaciones Derco
            $dercomotos_server = "72.10.48.174";
            $dercomotos_dbname = "derco_motos";
            $dercomotos_user = "derco_motos";
            $dercomotos_pass = "d6vnEpKCv7";

            

            // Abrimos la conx
            $conn = new mysqli($dercomotos_server, $dercomotos_user, $dercomotos_pass, $dercomotos_dbname);
                
            // Verificamos conexión
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            
            // Insertamos la Cotización en tabla general
            $sql = "INSERT INTO QUOTES (`firstname`, `lastname`, `rut`, `email`, `phone`, `address`, `comuna_personal`, `region_personal`, `model`, `model_number`, `concessionaire_name`, `concessionaire_comuna`, `concessionaire_region`, `value_vehicle`, `value_bono`, `value_total`, `url_cotizacion`, `pdf_url`, `export`, `origen`, `financiamiento`, `color`, `categoria`, `utm_source`, `utm_medium`, `utm_campaign`, `utm_content`, `utm_term`, `utm_code`) VALUES ('" . $firstname . "', '" . $lastname . "', '" . $rut . "', '" . $email . "', " . $phone . ", '" . $address . "', '" . $comuna_personal . "', '" . $region_personal . "', '" . $model . "', '" . $model_number . "', '" . $concessionaire_name . "', '" . $concessionaire_comuna . "', '" . $concessionaire_region . "', " . $value_vehicle . ", " . $value_bono . ", " . $value_total . ", '" . $url_cotizacion . "', '" . $pdf_url . "', " . $export . ", '" . $origen . "', '" . $financiamiento . "', '" . $color . "', '" . $categoria . "', '" . $utm_source . "', '" . $utm_medium . "', '" . $utm_campaign . "', '" . $utm_content . "', '" . $utm_term . "', '" . $utm_code . "')";
         
            if ($conn->query($sql) === true) {
              // $posted_data = $submission->get_posted_data();
              // print_r($posted_data);
                //Recuperamos el ID de Cotización
                $lastid = $conn->insert_id;

							//Envío formulario a plataforma DERCO

							$appData = [
                                'id_coti'               => $lastid,
                                'id_ces_plat'           => $_POST['id_ces_plat'],
                                'firstname'             => utf8_encode($firstname),
                                'lastname'              => utf8_encode($lastname),
                                'rut'                   => $rut,
                                'email'                 => $email,
                                'phone'                 => $phone,
                                'address'               => $address,
                                'comuna_personal'       => $comuna_personal,
                                'region_personal'       => $region_personal ,
                                'model'                 => $model,
                                'model_number'          => $model_number,
                                'concesionario_name'    => $concessionaire_name,
                                'concesionario_comuna'  => $concessionaire_comuna,
                                'concesionario_region'  => $concessionaire_region,
                                'year'                  => $moto_ano,
                                'value_vehicle'         => $value_vehicle,
                                'value_bono'            => $value_bono,
                                'value_total'           => $value_total,
                                'url_cotizacion'        => $url_cotizacion,
                                'pdf_url'               => $pdf_url,
                                'export'                => $export,
                                'formulario'            => $origen,
                                'brand'                 => $marca,
                                'value_total'           => $value_total,
                                'financiamiento'        => $financiamiento,
                                'color'                 => $color,
                                'categoria'             => $categoria,
                                'utm_source'            => $utm_source,
                                'utm_medium'            => $utm_medium,
                                'utm_campaign'          => $utm_campaign,
                                'utm_content'           => $utm_content,
                                'utm_term'              => $utm_term,
                                'utm_code'              => $utm_code,
                                'id_ces'                => $id_ces
							];

									
							$response = wp_remote_post('https://dercostats.cl/api/quotations', array(
								'method' => 'POST',
								'headers' => array(
									'cache-control' => 'no-cache',
									'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJjb3RpemFjaW9uIiwibmFtZSI6ImVsIGNvbXBpIiwiaWF0IjoxNTE2MjM5MDIyfQ.vZx2EGuHxCJ2mYfUd9o1EnoDhTYzKgsMd-HJoDJEw2w',
									// 'content-type' => 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
								),
								'body' => $appData
								)
							);

                    
                //$pdf_file_name = helpers::randomString(20);
                //$pdf_file_name = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 20);
                //$pdf_file_url = get_site_url() . '/pdf/' . $pdf_file_name . '.pdf';

                // Proceso de CSV
                // Generamos un ramdom que acompaña el nombre del CSV
                function generateRandomString($length = 10)
                {
                    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $charactersLength = strlen($characters);
                    $randomString = '';
                    for ($i = 0; $i < $length; $i++) {
                        $randomString .= $characters[rand(0, $charactersLength - 1)];
                    }
                    return $randomString;
                }
                $rel = generateRandomString(5);

                // Formato de nombre de CSV: MARCA_AAAAMMDD_XXX.CSV
                $csv_name = 'kymco_' . date('Ymd') . '_' . $rel . '.csv';
                $csv_path = ABSPATH . 'csv/';

                //creamos el CSV
                $csv = fopen($csv_path . $csv_name, "w+") or die("Unable to open file!");
                //declaramos heads del CSV
                $csv_heads = [
                    'id',
                    'fecha',
                    'simulador_clientes_rut',
                    'nombre',
                    'apellido',
                    'fonoFijo',
                    'fonoMovil',
                    'mail',
                    'calle',
                    'numero',
                    'comuna',
                    'ciudad',
                    'region',
                    'origen',
                    'origen2',
                    'marca',
                    'modelo',
                    'version',
                    'codigoVersion',
                    'versionColor',
                    'accesorios',
                    'precioVersion',
                    'partePagoMarca',
                    'partePagoModelo',
                    'partePagoPatente',
                    'partePagoAnio',
                    'partePagoValor',
                    'Nombre Ces',
                    'Direccion Ces',
                    'N Direccion Ces',
                    'Codigo Concesionario',
                    'Centro Concesionario',
                    'codigoConcesionario (DEALER PORTAL)',
                    'centroConcesionario(DEALER PORTAL)'
                ];
                //escribimos heads del CSV
                fwrite($csv, implode("|", $csv_heads));
                //declaramos valores del CSV
                $csv_valores = [
                    $lastid, //id
                    date('d-m-Y H:i'),//date_format(date_create($result->creation_date), 'd-m-Y H:i'), // fecha
                    $rut, // simulador_clientes_rut
                    $firstname, // nombre
                    $lastname, // apellido
                    substr($phone, -9), // fonoFijo
                    substr($phone, -9), // fonoMovil
                    $email, // mail
                    '', // calle
                    '', // numero
                    $comuna_personal, // comuna
                    '', // ciudad
                    $region_personal, // region
                    $url_cotizacion, // origen
                    get_site_url(), // origen2
                    $marca, // marca
                    $model, // modelo
                    $model_number, // version
                    '', // codigoVersion
                    $color, // versionColor
                    '', // accesorios
                    $value_total, // precioVersion
                    '', // partePagoMarca
                    '', // partePagoModelo
                    '', // partePagoPatente
                    '', // partePagoAnio
                    $financiamiento, // partePagoValor
                    $concessionaire_name, // Nombre Ces
                    '', // Direccion Ces
                    'Cotización Remota', // N Direccion Ces
                    $id_ces, // Codigo Concesionario
                    '', // Centro Concesionario
                    '', // codigoConcesionario (DEALER PORTAL)
                    '' // centroConcesionario(DEALER PORTAL)
                ];
                //Escribimos valores del CSV
                fwrite($csv, "\n" . implode("|", $csv_valores));
                fwrite($csv, "\n");
                fclose($csv);
                //terminamos de crear el CSV

                // empezamos a subir el CSV a FTP Derco
                $csv_folder = '/meat/Derco/cotizaciones-pp/';
                $conn_id = ftp_connect('ftp.derco.cl');
                $login = ftp_login($conn_id, 'meat', 'meat2011.');
                $put = ftp_put($conn_id, $csv_folder . $csv_name, $csv_path . $csv_name, FTP_ASCII);
                ftp_close($conn_id);
                
                // terminamos de subir el CSV a FTP Derco
            }
            // Cerramos conexión
            $conn->close();
        }
    }
};
// add the action 
add_action('wpcf7_mail_sent', 'action_wpcf7_mail_sent', 10, 1);

