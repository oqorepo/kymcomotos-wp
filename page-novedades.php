<?php get_template_part('includes/header'); ?>

<?php if(have_posts()) : ?>
<?php while(have_posts()) : the_post(); ?>
<a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail(); ?>
</a>
<?php endwhile; ?>
    
<?php if ( function_exists('b4st_pagination') ) { b4st_pagination(); } else if ( is_paged() ) { ?>
    <ul class="pagination">
    <li class="page-item older">
        <?php next_posts_link('<i class="fas fa-arrow-left"></i> ' . __('Previous', 'b4st')) ?></li>
<li class="page-item newer">
    <?php previous_posts_link(__('Next', 'b4st') . ' <i class="fas fa-arrow-right"></i>') ?></li>
</ul>
<?php } ?>

<?php
else :
get_template_part('./includes/loops/404');
endif;
?>

<?php get_template_part('includes/footer'); ?>
