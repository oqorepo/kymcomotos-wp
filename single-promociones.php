<?php get_template_part('includes/header'); ?>

<section class="bk-section">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_post_thumbnail('full', ['class'=>'w-100']); ?>
    <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
        <header class="m-5">
            <h2 class="text-center bk-title--red">
                <?php the_title()?>
            </h2>
        </header>
        <div class="container mb-5">
            <div class="row bk-section">
                <div class="col">
                    <?php
                    the_content();
                    wp_link_pages();
                    ?>
                </div>
            </div>
    </article>

<?php
  endwhile; wp_reset_postdata(); else :
    get_template_part('./includes/loops/404');
  endif;
?>

<div class="container">
    <div class="row pt-4">
        <div class="col">
            <?php previous_post_link('%link', '<i class="fas fa-fw fa-arrow-left"></i> Anterior: '.'%title'); ?>
        </div>
        <div class="col text-right">
            <?php next_post_link('%link', 'Siguiente: '.'%title' . ' <i class="fas fa-fw fa-arrow-right"></i>'); ?>
        </div>
    </div>
</div>

<?php get_template_part('includes/loops/slides/home-product'); ?>

<?php get_template_part('includes/footer'); ?>