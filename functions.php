<?php
/*
All the functions are in the PHP files in the `functions/` folder.
 */

require get_template_directory() . '/functions/cleanup.php';
require get_template_directory() . '/functions/setup.php';
require get_template_directory() . '/functions/enqueues.php';
require get_template_directory() . '/functions/develop.php';
require get_template_directory() . '/functions/custom-post.php';
require get_template_directory() . '/functions/disable-comments.php';
require get_template_directory() . '/functions/navbar.php';
require get_template_directory() . '/functions/widgets.php';
require get_template_directory() . '/functions/search-widget.php';
require get_template_directory() . '/functions/index-pagination.php';
require get_template_directory() . '/functions/single-split-pagination.php';
require get_template_directory() . '/functions/maps.php';
require get_template_directory() . '/functions/woocommerce-functions.php';

function the_breadcrumb()
{
    if (!is_home()) {
        echo '<span class="removed_link" title="';
        echo get_option('home');
        echo '">';
        bloginfo('name');
        echo "</span> » ";
        if (is_category() || is_single()) {
            the_category('title_li=');
            if (is_single()) {
                echo " » ";
                the_title();
            }
        } elseif (is_page()) {
            echo the_title();
        }
    }
}

// define the walker_nav_menu_start_el callback 
/*add_filter( 'walker_nav_menu_start_el', 'wpse_45647_add_custom_content', 10, 2 ); 
function wpse_45647_add_custom_content( $item_output, $item )
{
    // make filter magic happen here... 
    echo ""'<ul class="dropdown-menu depth_0 show">
    <div class="container" style="background:white;">
    <div class="row justify-content-between">
      <div class="col-sm-4 bk-col-cat">
        '. get_template_part('includes/loops/woo-category-menu'). '
      </div>
      <!-- <div class="col-sm">texto</div> -->
      <div class="col-sm-5 text-center pt-5">
        <img src="'. bloginfo('template_directory'). '/assets/img/auot-menu.png" alt="" style="" >
        <div class="text-center">
            <img src="'. bloginfo('template_directory'). '/assets/img/moto-menu.png" alt="" style="" >
          <h5 class="text-center"><span class="bk--title__i"> RENDIMIENTO Y ESTILO ÚNICO</span></h5>
          <p >Obtén un control sobre un estándar completamente nuevo. Aspecto llamativo, comodidad, poder y versatilidad, ofrecen ergonomía total, independiente del tipo de conducción que prefieras.</p>
          <a href="#cd-nav" style="width:200px;" class="nav-link align-self-center cd-nav-trigger bk--btn bk--btn__primary bk--btn__small">Conócela ></a>
        </div>
      </div>
    </div>
    </div>
  </ul>';
    return $array; 
};
add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );
function your_custom_menu_item ( $items, $args ) {
    if (is_single() && $args->theme_location == 'primary') {
        }
        $items .= '<li>Show whatever</li>';
    return $items;
}*/

// compatabilidad para subir CSV (wp 4.9.9 - 5.x)
function bp_mime_type($mime_types) {
    $mime_types['csv'] = 'text/csv';
    return $mime_types;
}
add_filter('upload_mimes', 'bp_mime_type', 1, 1);


