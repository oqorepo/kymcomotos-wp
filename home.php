<?php get_template_part('includes/header'); ?>

<section class="container mt-5">
    <div class="row mb-5">
    <?php if(have_posts()) : ?>
        <!--<h1>
            <?php _e('Category: ', 'b4st'); echo single_cat_title(); ?>
        </h1> -->
        <?php while(have_posts()) : the_post(); ?>
    
            <?php if(wp_is_mobile()) : ?>
            <div class="col-sm-4">
                <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card mt-5"); ?> >
                    <a href="<?php the_permalink();?>" class="link">
                        <img src="<?php the_post_thumbnail_url('medium'); ?>" class="w-100">
                        <div class="text-center">
                            <h2><?php echo the_title();?></h2>
                            <a class="info bk--btn bk--btn__primary bk--btn__small mt-3 p-2" href="<?php the_permalink();?>">Ver más ></a>
                        </div>
                    </a>
                </article>
            </div>
            <?php else : ?>
            <div class="col-sm-4">
                <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card mt-5"); ?> >
                    <!-- <header class="bk-loop-card--header">
                        <?php the_post_thumbnail(); ?>
                    </header>
                    <div class="bk-loop-card--content">
                        <h2>
                            <a href="<?php the_permalink(); ?>"> <?php the_title()?> </a>
                        </h2>
                    </div> -->

                    <div class="hovereffect">
                <img src="<?php the_post_thumbnail_url('medium'); ?>" class="img">
                        <a href="<?php the_permalink();?>" class="link">
                            <div class="overlay">
                                <h2><?php echo the_title();?></h2>
                                <a class="info bk--btn bk--btn__primary bk--btn__small" href="<?php the_permalink();?>">Ver más ></a>
                            </div>
                        </a>
                    </div>
                </article>
            </div>
            <?php endif; ?>
        
        <?php endwhile; ?>
    
        <?php if ( function_exists('b4st_pagination') ) {
            echo'
            <div class="container">
                <div class="row justify-content-center pt-5">
            ';
            b4st_pagination(); 
            echo'
                </div>
            </div>
            ';
        } else if ( is_paged() ) { ?>
            <div class="container p-5">
                <div class="row text-center">
                    <ul class="pagination">
                        <li class="page-item older">
                            <?php next_posts_link('<i class="fas fa-arrow-left"></i> ' . __('Previous', 'b4st')) ?>
                        </li>
                        <li class="page-item newer">
                            <?php previous_posts_link(__('Next', 'b4st') . ' <i class="fas fa-arrow-right"></i>') ?>
                        </li>
                    </ul>
                </div>
            </div>   
        <?php } ?>
    
    <?php
    else :
    get_template_part('./includes/loops/404');
    endif;
    ?>

    </div>

</section><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
