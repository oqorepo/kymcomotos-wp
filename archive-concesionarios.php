<?php get_template_part('includes/header'); ?>


<img src="<?php bloginfo('template_directory')?>/assets/img/bk-header-default.jpg" alt="" style="width:100%;">
<section id="concesionarios" class="container ces" style="padding-top:50px">
    <div class="row">
        <div class="bk--title mb-5">
            <div class="text-center ces-home--search__icons">
                <i class="fas fa-map-marker-alt" style="font-size:2.4em;padding-bottom:10px"></i>
            </div>
            <h2 class="text-center">Más de 30 concesionarios</h2>
        </div>
    </div>
    <div class="row barra-buscador">
        <div class="col-md-6 align-items-center">
            <h5 class="col-md-5 pt-3 float-left"> <i class="fas fa-map-marker-alt float-left" style="padding-right:5px"></i> Buscar por Región</h5>
            <div class="bk-dealer--select col-md-6 float-left">
                <?php 
                $taxonomies = get_terms( array(
                    'taxonomy' => 'ubicaciones',
                    'hide_empty' => true
                ) );
                
                if ( !empty($taxonomies) ) :
                    $output = '<select name="selector_regiones_ces" id="js_selector_regiones_ces" class="form-control" style="height:auto"><option>Seleccionar</option>';
                    foreach( $taxonomies as $category ) {
                        if( $category->parent == 0 ) {
                            $output.= '<optgroup label="'. esc_attr( $category->name ) .'">';
                            foreach( $taxonomies as $subcategory ) {
                                if($subcategory->parent == $category->term_id) {
                                $output.= '<option value="'.esc_attr(strtolower(str_replace(' ', '-',$subcategory->name ))).'">
                                    '. esc_html( $subcategory->name ) .'</option>';
                                }
                            }
                            $output.='</optgroup>';
                        }
                    }
                    $output.='</select>';
                    echo $output;
                endif;
                ?>
            </div>
        </div>
        <div class="col-md-6 align-items-center">
            <div class="col-md-6 float-left">
                <h5 class="float-left pt-3"> <i class="fas fa-crosshairs float-left" style="padding-right:5px"></i> Buscar en los alrededores</h5>
            </div>
            <form class="col-md-6 float-left" role="search" method="get" id="searchformCes" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <div class="input-group mb-3">
                    <input type="text" name="s" id="s" class="wpcf7-form-control form-control required" value="<?php echo get_search_query(); ?>" placeholder="Ingresar comuna o nombre del Concesionario">
                    <div class="input-group-append" style="background-color: #495057;border-top-right-radius: 6px;border-bottom-right-radius: 6px;">
                        <button id="searchsubmitCes" value="<?php esc_attr_x('Search', 'b4st') ?>" class="btn btn-outline-secondary" type="submit"><i class="fas fa-search" style="background-color:#495057;"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row cesmapcontainer">
        <div class="col-md-6">
            <div class="bk-dealer--selector text-center w-100" id="style-2">
                <div class="bk-dealer--detail p-2">
                    <?php
                    $zona_norte_args = array(
                        'post_type'      => 'concesionarios',
                        'posts_per_page' => -1
                    );
                        $zona_norte = new WP_Query( $zona_norte_args ); 
                    ?>
                    <?php if ( $zona_norte->have_posts() ) :?>
                        <ul class="bk-distribuidores">
                        <?php while ($zona_norte->have_posts()) : $zona_norte->the_post(); 
                            $location = get_field('cn-map');
                            $title = get_the_title();
                        ?>
                            <li class="<?php
                            $terms = get_the_terms( $post->ID, 'ubicaciones' );
                            if (!empty($terms)) {
                                foreach($terms as $term) {
                                    if (!get_term_children($term->term_id, 'ubicaciones')) {
                                        echo strtolower(str_replace(' ', '-',$term->name));
                                    }
                                }
                            }
                            ?> d-none">
                                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                                    <a class="bk-marker" data-name="<?php echo strtolower(str_replace(' ', '', the_title('', '', false))); ?>" href="#" rel="bookmark" data-marker="0" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                                        <h4> <?php the_title(); ?></h4>
                                        <p class="address"><?php echo $location['address']; ?></p>
                                    </a>
                                </div>
                            </li>
                            <?php ?>
                        <?php endwhile; wp_reset_postdata();?>
                        </ul>
                    <?php endif;?>
                </div><!-- Detail card-->
            </div>
            <!--INICIO COLUMNA 2 CONTENIDO-->
            <div class="bk-dealer--contenido d-none bk-no-padding">
                <ul class="bk-dealer--card">
                <?php $data_args = array(
                    'post_type'      => 'concesionarios',
                    'posts_per_page' => -1
                );
                $data_loop = new WP_Query( $data_args ); 
                ?>
                <?php if ( $data_loop->have_posts() ) :?>
                <?php while ($data_loop->have_posts()) : $data_loop->the_post();
                    $location = get_field('cn-map');
                    ?>
                    <li class="bk-dealer--content" id="<?php echo strtolower(str_replace(' ', '', the_title('', '', false))); ?>">
                    <?php if( have_rows('cn-datos') ): while( have_rows('cn-datos') ): the_row();
                    $image = get_sub_field('cn-img');
                    $link = get_sub_field('cn-tel');
                    $mail = get_sub_field('cn-email'); 
                    $colors = get_sub_field('cn-service-type'); 
                    $atention_times = get_field('cn-time');
                    ?>
                        <?php if( !empty($image) ): ?>
                        <div class="bk-dealer--card__img" style="background:url('<?php echo $image['url']; ?>');">
                        </div>
                        <?php endif; ?>
                        <?php if( !empty($mail) ): ?>
                        <div class="bk-dealer--card__txt">
                            <h4><?php the_title(); ?></h4>
                            <p class="cn-address"><b>Dirección:</b><br><?php echo $location['address']; ?></p>
                            <?php if( have_rows('cn-tel') ): ?>
                            <p class="cn-tel"><b>Teléfonos:</b><br>
                                <?php while( have_rows('cn-tel') ): the_row();
                                $content = get_sub_field('cn-tel-rp');
                                echo ' / ';
                            ?>
                                <a href="<?php echo $content ?>"> <?php echo $content ?> </a>
                                <?php endwhile; ?>
                            </p>
                            <?php endif; ?>
                            <?php if( have_rows('cn-email') ): ?>
                            <p class="cn-email"><b>Email:</b><br>
                                <?php while( have_rows('cn-email') ){
                                    the_row(); 
                                    $mailings = get_sub_field('cn-email-rp');
                                    echo '<a href="mailto:'.$mailings.'">'.$mailings.'</a>';
                                    break; 
                                } 
                            ?>
                            </p>
                            <?php endif; ?>
                            <?php if( $atention_times ): ?>
                                <p class="cn-time"><b>Horario de atención:</b><br><?php echo $atention_times; ?></p>
                            <?php endif; ?>
                            <?php if( $colors ): ?>
                                <p class="cn-service"><b>Tipos de servicios:</b><br></p>
                            <ul class="d-flex">
                                <?php foreach( $colors as $color ): ?>
                                <li class="cn-<?php echo $color; ?> cn-service-icon"><p><?php echo $color; ?></p></li>
                                <?php endforeach; ?>
                            </ul>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                    <?php endwhile; endif; ?>
                    </li>
                <?php endwhile; wp_reset_postdata(); ?>
                <?php endif;?>
                </ul>
            </div>
            <!--FIN COLUMNA 2 CONTENIDO-->
        </div>
        <div class="col-md-6">
            <!--INICIO COLUMNA 3 MAP-->
            <div class="ces-dealer--map d-none bk-no-padding">
                <?php  $args = array(
                    'post_type'      => 'concesionarios',
                    'posts_per_page' => -1
                );
                $the_query = new WP_Query($args);
                while ( $the_query->have_posts() ) : $the_query->the_post();
                $location = get_field('cn-map');
                if ($location):
                ?>
                <div class="bk-attr" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                </div>
                <?php endif; endwhile; wp_reset_postdata();?>
                <?php if( !empty($location) ): ?>
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>">
                        <h4><a href="<?php the_permalink(); ?>" rel="bookmark"> <?php the_title(); ?></a></h4> <!-- Output the title -->
                        <p class="address"><?php echo $location['address']; ?></p> <!-- Output the address -->
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <!-- FINCOLUMNA 3 MAP-->
        </div>
    </div>
</section>

<?php get_template_part('includes/section-c'); ?>
<?php get_template_part('includes/map'); ?>
<?php get_template_part('includes/footer'); ?>