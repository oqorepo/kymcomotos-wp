<?php /* Template Name: Contacto OK */?>

<?php get_template_part('includes/header'); ?>

<section class="container mt-5 mb-5 bk-home--promo">

    <div class="row">
        <div class="col-12">
            <div class="text-center bk--title">
                <h2 class="text-center"><i class="fas fa-check" style="color:#41b451"></i> ¡Buena elección <strong id="nombre"></strong>!</h2>
                <p class="text-center">Ya estás a un paso de tener la moto que deseas, pronto te contáctaremos.</p>
            </div>
        </div>
    </div>

    <div class="row" style="padding-top:20px">
            <div class="col-md-6 text-center">
                <div class="bk-cot--card">
                    <div class="bk-cot--card__img" style="height:250px;width:auto;">
                        <img id="img-moto" class="xw-100" src="" style="height:250px;width:auto;">
                    </div>
                    <div class="bk-cot--card__content">
                        <div class="bk-cot--card__content--price">
                            <h4 id="m_modelo" class="name_moto">
                                
                            </h4>
                            <div class="bono">
                                <small>Precio Normal: <span id="precio_n" style="color:#41b451; font-weight:900">$ 3.499.900</span></small>
                            </div>
                            <div class="normal bk-shop--banner__price-normal">
                                <h2 id="precio_f">$ 3.199.900</h2>
                            </div>
                            <div class="bono bk-shop--banner__price-bono">
                                <h4 id="precio_b">300.000</h4>
                            </div>
                            <div class="bono-autofin bk-shop--banner__price-bono">
                                <h4>
                                    Bono de Financiamiento: $ 
                                </h4>
                            </div>
                        </div>
                        <div class="bk-cot--card__content--cuote autofin-evaluate" id="cuota_cae">
                            <div class="autofin-container">
                                <div class="title black">
                                    <span class="cae">36 Cuotas (CAE 52.34%)</span>
                                </div>
                                <div class="price">
                                    $ 124.463
                                </div>
                            </div>
                        </div>
                        <div class="p-2">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg-footer.jpg" alt="kymco" style="max-width:120px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 grey-box">
                <div class="ty01-txt">
                    Recuerda que tu concesionario seleccionado es:
                </div>
                <div class="ty02-txt">
                    <p id="ces_name"></p>
                    <i class="fas fa-map icon-red"></i> <span id="direccion"></span>
                </div>
                <div class="ty01-txt">
                    Horarios de atención:
                </div>
                <div class="ty02-txt">
                    <strong>Ventas</strong><br>
                    <p id="ces_horario">Lunes, Martes, Miércoles, Jueves, Viernes <strong>de 09:00:00 a 20:00:00</strong></p>
                    <i class="fas fa-phone-square icon-red"></i> <span id="telefono">552717777</span> <span id="telefono2"></span> <span id="telefono3"></span>
                </div>
                <div class="link-concesionaria">
                    <a id="g_map" href="" alt="link mapa concesionaria" target="_blank">
                        <i class="fas fa-map-marker-alt"></i> ¿Como llegar a tu concesionario?
                    </a>
                </div>
            </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="autofinModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:9999999999;" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" >
    <div class="modal-content" style="padding:0;">
      <div class="modal-header" style="padding:5px 10px 0px;">
      	<h5 class="modal-title">Evaluación de Crédito Autofin</h5>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body" style="padding:0;">
          <iframe id="autofinIframe" src="" width="100%" height="740px" frameborder="0" allowtransparency="true"></iframe>  
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php get_template_part('includes/footer'); ?>