<?php 
/*
Template Name: Home Page
*/
?>
<?php get_template_part('includes/header'); ?>

<?php get_template_part('includes/loops/slides/home-product'); ?>

<?php get_template_part('includes/section-c'); ?>

<?php get_template_part('includes/section-v'); ?>

<section class="container mt-5 mb-5 bk-home--promo d-none d-md-block">
    <div class="row">
        <div class="col-12">
            <div class="bk--title">
                <h2 class="text-center">Mucho más<span class="bk--title__i"> que motos</span></h2>
                <p class="text-center">- Better Than Best -</p>
            </div>
        </div>
        <div class="col-md-4 text-center pt-2 pb-2">
            <div class="bk-home--promo__card">
                <a href="<?php echo site_url('/concesionarios') ?>" class="bk-home--promo__link">
                    <figure>
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/promo-1-bg.png" alt="" class="w-100">
                    </figure>
                    <div class="bk-home--promo__content">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-tools.png" alt="">
                        <h5>Concesionarios</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 text-center pt-2 pb-2">
            <div class="bk-home--promo__card">
                <a href="http://www.dercomotos.cl/escuela-de-manejo/" class="bk-home--promo__link">
                    <figure>
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/promo-2-bg.png" alt="" class="w-100">
                    </figure>
                    <div class="bk-home--promo__content">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-motorbike.png" alt="">
                        <h5>Escuela Dercomotos</h5>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 text-center pt-2 pb-2">
            <div class="bk-home--promo__card">
                <a href="<?php echo site_url('/novedades') ?>" class="bk-home--promo__link" target="_blank">
                    <figure>
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/promo-3-bg.png" alt="" class="w-100">
                    </figure>
                    <div class="bk-home--promo__content">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-helmet.png" alt="">
                        <h5>Eventos Especiales</h5>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('includes/loops/slides/novedades'); ?>

<?php get_template_part('includes/footer'); ?>
