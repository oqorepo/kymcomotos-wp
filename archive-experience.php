<?php get_template_part('includes/header'); ?>
<img src="<?php bloginfo('template_directory')?>/assets/img/experience_hero_1.jpg" alt="" style="width:100%;">

<?php 
$today = date("Ymd");
$taxonomy = 'eventype';
$terms_args = array(
  'orderby' => 'id',
  'hide_empty' => true,
);
$cats = get_terms( $taxonomy, $terms_args );
?>

<section class="container-fluid pb-5 pt-5 experience-future">
  <div class="container">
    <div class="bk--title mb-4">
      <h2 class="text-center">Eventos<span class="bk--title__i"> próximos</span></h2>
      <p class="text-center">- Better Than Best -</p>
    </div>
  </div>

<?php 
foreach ($cats as $cat) :
  $cat_id= $cat->term_id;
  $term_slug = $cat->slug;
  $custom_query = new WP_Query(array(
    'post_type'         => 'experience',
    'posts_per_page' => 6,
    'tax_query' => array(
      array(
        'taxonomy' => 'eventype',
        'field'    => 'slug',
        'terms'    => $term_slug
      ),
    ),
    'meta_query'   => array(
      array(
          'key'       => 'ex_fecha_del_evento',
          'value'     => $today,
          'compare'   => '>=',
          'type'      => 'DATE',
      ),
    ),
    'meta_key'               => 'ex_fecha_del_evento',
    'orderby'                => 'meta_value',
    'order'                  => 'ASC'
  ));

  if ( $custom_query->have_posts() ) :
    $cognome_nome = get_field('ex_tax_img', $cat->taxonomy.'_'.$cat->term_id);
    $term_link = get_term_link( $cat );
?>
<div class="container experience-future--child">
  <div class="row bk-home--events__cards--row pt-4">
    <div class="col-sm-6 bk-home--events__cards--img">
      <?php echo '<img src="'.$cognome_nome['sizes']['large'].'" class="w-100">';?>
    </div>
    <div class="col-sm-6 bk-home--events__cards--text">

      
    <div class="container ">
    <?php echo '<h2 class="text-center"><a href="'. esc_url( $term_link ) . '">'.$cat->name.'</a></h2>';?>
      <?php while( $custom_query->have_posts() ) : $custom_query->the_post();  
      $ex_date = get_field('ex_fecha_del_evento', false, false);
      ?>
        <ul class="row bk-event--loop pb-2">
            <li class="col-sm-9 bk-event--loop__info">
                <a href="<?php the_permalink();?>">
                    <p class="bk-event--loop__info--title"><?php echo the_title();?></p>
                </a>
              <a href="<?php the_permalink();?>"><small>Ver más</small></a>
            </li>
            <li class="col-sm-3 bk-event--loop__date">
                <p>
                <?php 
                    $event_date = DateTime::createFromFormat('Ymd', $ex_date);
                    echo $event_date->format('d/m/Y'); 
                ?>
                </p>
            </li>
        </ul>
      <?php endwhile;
      wp_reset_postdata();
      echo '</div>';
    endif; ?>
    </div></div><hr></div>
    <?php endforeach; ?>
  </div>
</section>

<section class="container-fluid pb-5 pt-5" style="background:#f4f4f4;">
    <div class="container">
      <div class="bk--title mb-4">
        <h2 class="text-center">Eventos<span class="bk--title__i"> realizados</span></h2>
        <p class="text-center">- Better Than Best -</p>
      </div>
      <hr>
    </div>
    <div class="container bk-home--events__cards--container">
<?php
foreach ($cats as $cat) :
  $cat_id= $cat->term_id;
  $term_slug = $cat->slug;
  $custom_query = new WP_Query(array(
    'post_type'         => 'experience',
    'posts_per_page' => 6,
    'tax_query' => array(
      array(
        'taxonomy' => 'eventype',
        'field'    => 'slug',
        'terms'    => $term_slug,
      ),
    ),
    'meta_query'             => array(
      array(
          'key'       => 'ex_fecha_del_evento',
          'value'     => $today,
          'compare'   => '<=',
          'type'      => 'DATE',
      ),
    ),
    'meta_key'               => 'ex_fecha_del_evento',
    'orderby'                => 'meta_value',
    'order'                  => 'ASC'
  ));
  $cognome_nome = get_field('ex_tax_img', $cat->taxonomy.'_'.$cat->term_id);
  $term_link = get_term_link( $cat );
  if ( $custom_query->have_posts() ) :?>
  <div class="row bk-home--events__cards--row pt-4">
    <div class="col-sm-4 text-center">
      <?php echo '<img src="'.$cognome_nome['sizes']['thumbnail'].'" alt=""><h3 class="text-center"><a href="'. esc_url( $term_link ) . '">'.$cat->name.'</a></h3>';?>
    </div>
    <div class="col-sm-8">
  <?php
    
      echo '<div id="experience-owl" class="owl-carousel owl-theme experience-owl">';
      while( $custom_query->have_posts() ) : $custom_query->the_post();  
      $ex_date = get_field('ex_fecha_del_evento', false, false);
      $ex_date = new DateTime($ex_date);
      ?>
          <article role="article" id="post_<?php the_ID()?>" <?php post_class("bk-loop-card"); ?> >
            <div class="hovereffect">
              <img src="<?php the_post_thumbnail_url('medium'); ?>" class="img">
                  <a href="<?php the_permalink();?>" class="link">
                      <div class="overlay">
                          <h2><?php echo the_title();?></h2>
                          <!-- <a class="info bk--btn bk--btn__primary bk--btn__small" href="<?php the_permalink();?>">Ver más ></a> -->
                      </div>
                  </a>
              </div>
            <!-- <p><?php echo $ex_date->format('j M Y'); ?></p> -->
          </article>
      <?php endwhile;
      echo '</div>';
      wp_reset_postdata();
    endif;
    echo '</div></div><hr>';
  endforeach;
  echo '</div></section>';
get_template_part('includes/footer'); 
?>